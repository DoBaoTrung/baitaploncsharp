﻿namespace QuanLyCuaHangDienThoai
{
    partial class MasterLayout
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.MenuItem_File = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_File_TrangChu = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_File_DangXuat = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_File_Thoat = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_KhachHang = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_KhachHang_QuanLy = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_KhachHang_DonHang = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_NhanVien_ThemMoi = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_NhanVien_QuanLy = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_KhoHang = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_KhoHang_QuanLy = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_TroGiup = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_TroGiup_HuongDan = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_TroGiup_PhienBan = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_TroGiup_TacGia = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_KhoHang_BaoCao = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItem_File,
            this.MenuItem_KhachHang,
            this.MenuItem_NhanVien_ThemMoi,
            this.MenuItem_KhoHang,
            this.MenuItem_TroGiup});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip";
            // 
            // MenuItem_File
            // 
            this.MenuItem_File.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItem_File_TrangChu,
            this.MenuItem_File_DangXuat,
            this.MenuItem_File_Thoat});
            this.MenuItem_File.Name = "MenuItem_File";
            this.MenuItem_File.Size = new System.Drawing.Size(37, 20);
            this.MenuItem_File.Text = "File";
            // 
            // MenuItem_File_TrangChu
            // 
            this.MenuItem_File_TrangChu.Name = "MenuItem_File_TrangChu";
            this.MenuItem_File_TrangChu.Size = new System.Drawing.Size(127, 22);
            this.MenuItem_File_TrangChu.Text = "Trang chủ";
            this.MenuItem_File_TrangChu.Click += new System.EventHandler(this.MenuItem_File_TrangChu_Click);
            // 
            // MenuItem_File_DangXuat
            // 
            this.MenuItem_File_DangXuat.Name = "MenuItem_File_DangXuat";
            this.MenuItem_File_DangXuat.Size = new System.Drawing.Size(127, 22);
            this.MenuItem_File_DangXuat.Text = "Đăng xuất";
            this.MenuItem_File_DangXuat.Click += new System.EventHandler(this.MenuItem_File_DangXuat_Click);
            // 
            // MenuItem_File_Thoat
            // 
            this.MenuItem_File_Thoat.Name = "MenuItem_File_Thoat";
            this.MenuItem_File_Thoat.Size = new System.Drawing.Size(127, 22);
            this.MenuItem_File_Thoat.Text = "Thoát";
            this.MenuItem_File_Thoat.Click += new System.EventHandler(this.MenuItem_File_Thoat_Click);
            // 
            // MenuItem_KhachHang
            // 
            this.MenuItem_KhachHang.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItem_KhachHang_QuanLy,
            this.MenuItem_KhachHang_DonHang});
            this.MenuItem_KhachHang.Name = "MenuItem_KhachHang";
            this.MenuItem_KhachHang.Size = new System.Drawing.Size(82, 20);
            this.MenuItem_KhachHang.Text = "Khách hàng";
            // 
            // MenuItem_KhachHang_QuanLy
            // 
            this.MenuItem_KhachHang_QuanLy.Name = "MenuItem_KhachHang_QuanLy";
            this.MenuItem_KhachHang_QuanLy.Size = new System.Drawing.Size(180, 22);
            this.MenuItem_KhachHang_QuanLy.Text = "Quản lý khách hàng";
            this.MenuItem_KhachHang_QuanLy.Click += new System.EventHandler(this.MenuItem_KhachHang_QuanLy_Click);
            // 
            // MenuItem_KhachHang_DonHang
            // 
            this.MenuItem_KhachHang_DonHang.Name = "MenuItem_KhachHang_DonHang";
            this.MenuItem_KhachHang_DonHang.Size = new System.Drawing.Size(180, 22);
            this.MenuItem_KhachHang_DonHang.Text = "Đơn hàng";
            this.MenuItem_KhachHang_DonHang.Click += new System.EventHandler(this.MenuItem_KhachHang_DonHang_Click);
            // 
            // MenuItem_NhanVien_ThemMoi
            // 
            this.MenuItem_NhanVien_ThemMoi.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItem_NhanVien_QuanLy});
            this.MenuItem_NhanVien_ThemMoi.Name = "MenuItem_NhanVien_ThemMoi";
            this.MenuItem_NhanVien_ThemMoi.Size = new System.Drawing.Size(73, 20);
            this.MenuItem_NhanVien_ThemMoi.Text = "Nhân viên";
            // 
            // MenuItem_NhanVien_QuanLy
            // 
            this.MenuItem_NhanVien_QuanLy.Name = "MenuItem_NhanVien_QuanLy";
            this.MenuItem_NhanVien_QuanLy.Size = new System.Drawing.Size(180, 22);
            this.MenuItem_NhanVien_QuanLy.Text = "Quản lý nhân viên";
            this.MenuItem_NhanVien_QuanLy.Click += new System.EventHandler(this.MenuItem_NhanVien_QuanLy_Click);
            // 
            // MenuItem_KhoHang
            // 
            this.MenuItem_KhoHang.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItem_KhoHang_QuanLy,
            this.MenuItem_KhoHang_BaoCao});
            this.MenuItem_KhoHang.Name = "MenuItem_KhoHang";
            this.MenuItem_KhoHang.Size = new System.Drawing.Size(70, 20);
            this.MenuItem_KhoHang.Text = "Kho hàng";
            // 
            // MenuItem_KhoHang_QuanLy
            // 
            this.MenuItem_KhoHang_QuanLy.Name = "MenuItem_KhoHang_QuanLy";
            this.MenuItem_KhoHang_QuanLy.Size = new System.Drawing.Size(180, 22);
            this.MenuItem_KhoHang_QuanLy.Text = "Quản lý kho hàng";
            this.MenuItem_KhoHang_QuanLy.Click += new System.EventHandler(this.MenuItem_KhoHang_QuanLy_Click);
            // 
            // MenuItem_TroGiup
            // 
            this.MenuItem_TroGiup.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItem_TroGiup_HuongDan,
            this.MenuItem_TroGiup_PhienBan,
            this.MenuItem_TroGiup_TacGia});
            this.MenuItem_TroGiup.Name = "MenuItem_TroGiup";
            this.MenuItem_TroGiup.Size = new System.Drawing.Size(63, 20);
            this.MenuItem_TroGiup.Text = "Trợ giúp";
            // 
            // MenuItem_TroGiup_HuongDan
            // 
            this.MenuItem_TroGiup_HuongDan.Name = "MenuItem_TroGiup_HuongDan";
            this.MenuItem_TroGiup_HuongDan.Size = new System.Drawing.Size(134, 22);
            this.MenuItem_TroGiup_HuongDan.Text = "Hướng dẫn";
            // 
            // MenuItem_TroGiup_PhienBan
            // 
            this.MenuItem_TroGiup_PhienBan.Name = "MenuItem_TroGiup_PhienBan";
            this.MenuItem_TroGiup_PhienBan.Size = new System.Drawing.Size(134, 22);
            this.MenuItem_TroGiup_PhienBan.Text = "Phiên bản";
            // 
            // MenuItem_TroGiup_TacGia
            // 
            this.MenuItem_TroGiup_TacGia.Name = "MenuItem_TroGiup_TacGia";
            this.MenuItem_TroGiup_TacGia.Size = new System.Drawing.Size(134, 22);
            this.MenuItem_TroGiup_TacGia.Text = "Tác giả";
            // 
            // MenuItem_KhoHang_BaoCao
            // 
            this.MenuItem_KhoHang_BaoCao.Name = "MenuItem_KhoHang_BaoCao";
            this.MenuItem_KhoHang_BaoCao.Size = new System.Drawing.Size(180, 22);
            this.MenuItem_KhoHang_BaoCao.Text = "Báo cáo";
            this.MenuItem_KhoHang_BaoCao.Click += new System.EventHandler(this.MenuItem_KhoHang_BaoCao_Click);
            // 
            // MasterLayout
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "MasterLayout";
            this.Load += new System.EventHandler(this.MasterLayout_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_File;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_File_TrangChu;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_File_DangXuat;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_File_Thoat;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_KhachHang;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_KhachHang_QuanLy;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_KhachHang_DonHang;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_NhanVien_ThemMoi;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_NhanVien_QuanLy;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_KhoHang;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_KhoHang_QuanLy;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_TroGiup;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_TroGiup_HuongDan;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_TroGiup_PhienBan;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_TroGiup_TacGia;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_KhoHang_BaoCao;
    }
}

