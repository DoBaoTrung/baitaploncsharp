﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyCuaHangDienThoai.Model
{
    public class TrangThaiDonHang : Model
    {
        public DataTable GetTrangThaiDataTable()
        {
            return this.GetDataTableByQuery("SELECT * FROM TrangThaiDonHang");
        }
    }
}
