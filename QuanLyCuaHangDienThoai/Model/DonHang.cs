﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyCuaHangDienThoai.Model
{
    class DonHang : Model
    {
        public string Id;
        public string NgayLap;
        public string KhachHangId;
        public string NhanVienLapId;
        public string TongHoaDon; 
        public string TrangThaiId;
        public string TienKhachDua;
        public string GhiChu;

        public DataTable GetDonHangDataTable()
        {
            return this.GetDataTableByQuery("SELECT * FROM View_DonHang");
        }

        public DataTable GetKetQuaTimKiemDataTable(string searchString)
        {
            string query = "EXEC DonHang_Search @s = N'" + searchString + "'";
            return this.GetDataTableByQuery(query);
        }

        public DonHang Forge(string id)
        {
            SqlDataReader result = this.GetQueryResult("SELECT * FROM DonHang WHERE id = " + id);
            result.Read();
            this.Id = id;
            this.NgayLap = result[1].ToString();
            this.KhachHangId = result[2].ToString();
            this.NhanVienLapId = result[3].ToString();
            this.TongHoaDon = result[4].ToString();
            this.TrangThaiId = result[5].ToString();
            this.TienKhachDua = result[6].ToString();
            this.GhiChu = result[7].ToString();
            return this;
        }

        public bool Update(object[] args)
        {
            using (SqlCommand cmd = new SqlCommand("DonHang_Update", this.Cnn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@id", args[0]));
                cmd.Parameters.Add(new SqlParameter("@ngay_lap", args[1]));
                cmd.Parameters.Add(new SqlParameter("@khach_hang_id", args[2]));
                cmd.Parameters.Add(new SqlParameter("@nhan_vien_lap_id", args[3]));
                cmd.Parameters.Add(new SqlParameter("@tong_don_hang", args[4]));
                cmd.Parameters.Add(new SqlParameter("@trang_thai_id", args[5]));
                cmd.Parameters.Add(new SqlParameter("@tien_khach_dua", args[6]));
                cmd.Parameters.Add(new SqlParameter("@ghi_chu", args[7]));

                if (cmd.ExecuteNonQuery() > 0)
                {
                    return true;
                }
                return false;
            }
        }

        public int Insert(object[] args)
        {
            using (SqlCommand cmd = new SqlCommand("DonHang_Insert", this.Cnn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ngay_lap", args[1]));
                cmd.Parameters.Add(new SqlParameter("@khach_hang_id", args[2]));
                cmd.Parameters.Add(new SqlParameter("@nhan_vien_lap_id", args[3]));
                cmd.Parameters.Add(new SqlParameter("@tong_don_hang", args[4]));
                cmd.Parameters.Add(new SqlParameter("@trang_thai_id", args[5]));
                cmd.Parameters.Add(new SqlParameter("@tien_khach_dua", args[6]));
                cmd.Parameters.Add(new SqlParameter("@ghi_chu", args[7]));

                int lastInsertedId = int.Parse(cmd.ExecuteScalar().ToString());
               
                return lastInsertedId;
            }
        }
        public bool Delete(string id)
        {
            using (SqlCommand cmd = new SqlCommand("DonHang_Delete", this.Cnn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@id", int.Parse(id)));

                if (cmd.ExecuteNonQuery() > 0)
                {
                    return true;
                }
                return false;
            }
        }
    }
}
