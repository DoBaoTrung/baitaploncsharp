﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyCuaHangDienThoai.Model
{
    public class NhanVien : Model
    {
        public string Id;
        public string HoTen;

        public DataTable GetNhanVienDataTable()
        {
            return this.GetDataTableByQuery("SELECT * FROM View_NhanVien");
        }

        public DataTable GetNhanVienSearchDataTable(string s)
        {
            return this.GetDataTableByQuery("EXEC NhanVien_Search @s=N'" + s + "'");
        }

        public NhanVien Forge(string id)
        {
            
            SqlDataReader result = GetQueryResult("SELECT * FROM NhanVien WHERE id = " + id);
            result.Read();

            this.Id = result[0].ToString();
            this.HoTen = result[1].ToString();

            return this;
        }

        public bool Update(object[] args)
        {
            using (SqlCommand cmd = new SqlCommand("NhanVien_Update", this.Cnn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@id", args[0]));
                cmd.Parameters.Add(new SqlParameter("@ho_ten", args[1]));
                cmd.Parameters.Add(new SqlParameter("@gioi_tinh", args[2]));
                cmd.Parameters.Add(new SqlParameter("@dien_thoai", args[3]));
                cmd.Parameters.Add(new SqlParameter("@ngay_sinh", args[4]));
                cmd.Parameters.Add(new SqlParameter("@dia_chi", args[5]));
                cmd.Parameters.Add(new SqlParameter("@chuc_vu_id", args[6]));
                cmd.Parameters.Add(new SqlParameter("@luong_co_ban", args[7]));

                if (cmd.ExecuteNonQuery() > 0)
                {
                    return true;
                }
                return false;
            }

        }
        public bool Insert(object[] args)
        {
            using (SqlCommand cmd = new SqlCommand("NhanVien_Insert", this.Cnn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ho_ten", args[1]));
                cmd.Parameters.Add(new SqlParameter("@gioi_tinh", args[2]));
                cmd.Parameters.Add(new SqlParameter("@dien_thoai", args[3]));
                cmd.Parameters.Add(new SqlParameter("@ngay_sinh", args[4]));
                cmd.Parameters.Add(new SqlParameter("@dia_chi", args[5]));
                cmd.Parameters.Add(new SqlParameter("@chuc_vu_id", args[6]));
                cmd.Parameters.Add(new SqlParameter("@luong_co_ban", args[7]));

                if (cmd.ExecuteNonQuery() > 0)
                {
                    return true;
                }
                return false;
            }
        }
        public bool Delete(string id)
        {
            using (SqlCommand cmd = new SqlCommand("NhanVien_Delete", this.Cnn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@id", int.Parse(id)));

                if (cmd.ExecuteNonQuery() > 0)
                {
                    return true;
                }
                return false;
            }
        }
    }
}
