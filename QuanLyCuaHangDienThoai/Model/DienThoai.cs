﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyCuaHangDienThoai.Model
{
    public class DienThoai : Model
    {
        public string Id;
        public string Gia;
        public string Ten;

        public DienThoai Forge(string id)
        {
            this.Id = id;

            SqlDataReader result = this.GetQueryResult("SELECT * FROM DienThoai WHERE id = '" + id + "'");
            result.Read();

            this.Ten = result[2].ToString();
            this.Gia = result[3].ToString();
            return this;
        }

        public static string GetGiaText(object gia)
        {
            gia = gia.ToString();
            return Convert.ToDecimal(gia).ToString("#,##0") + "đ";
        }

        public DataTable GetDienThoaiDataTable()
        {
            return this.GetDataTableByQuery("SELECT * FROM View_DienThoai");
        }

        public DataTable GetDienThoaiDataTableRaw()
        {
            return this.GetDataTableByQuery("SELECT * FROM DienThoai");
        }

        public DataTable GetKetQuaTimKiemDataTable(string searchString)
        {
            string query = "EXEC DienThoai_Search @s = N'" + searchString + "'";
            return this.GetDataTableByQuery(query);
        }
        public Image GetImageById(string id)
        {
            string query = "SELECT anh FROM DienThoai WHERE id = " + id;

            using (SqlCommand cmd = new SqlCommand(query, Cnn))
            {
                SqlDataAdapter dp   = new SqlDataAdapter(cmd);
                DataSet ds          = new DataSet("MyImages");

                byte[] imageData = new byte[0];

                dp.Fill(ds, "MyImages");
                DataRow myRow;
                myRow = ds.Tables["MyImages"].Rows[0];

                imageData = (byte[]) myRow["anh"];

                MemoryStream stream = new MemoryStream(imageData);

                return Image.FromStream(stream);
            }
        }

        public bool Update(object[] args, byte[] image)
        {
            using (SqlCommand cmd = new SqlCommand("DienThoai_Update", this.Cnn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@id", args[0]));
                cmd.Parameters.Add(new SqlParameter("@ma_sku", args[1]));
                cmd.Parameters.Add(new SqlParameter("@ten", args[2]));
                cmd.Parameters.Add(new SqlParameter("@gia", args[3]));
                cmd.Parameters.Add(new SqlParameter("@so_luong", args[4]));
                cmd.Parameters.Add(new SqlParameter("@thong_so", args[5]));
                cmd.Parameters.Add(new SqlParameter("@mau_sac_id", args[6]));
                cmd.Parameters.Add(new SqlParameter("@hang_id", args[7]));
                cmd.Parameters.Add(new SqlParameter("@ngay_phat_hanh", args[8]));
                cmd.Parameters.Add(new SqlParameter("@anh", image));

                if (cmd.ExecuteNonQuery() > 0)
                {
                    return true;
                }
                return false;
            }

        }
        public bool Insert(object[] args, byte[] image)
        {
            using (SqlCommand cmd = new SqlCommand("DienThoai_Insert", this.Cnn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ma_sku", args[1]));
                cmd.Parameters.Add(new SqlParameter("@ten", args[2]));
                cmd.Parameters.Add(new SqlParameter("@gia", args[3]));
                cmd.Parameters.Add(new SqlParameter("@so_luong", args[4]));
                cmd.Parameters.Add(new SqlParameter("@thong_so", args[5]));
                cmd.Parameters.Add(new SqlParameter("@mau_sac_id", args[6]));
                cmd.Parameters.Add(new SqlParameter("@hang_id", args[7]));
                cmd.Parameters.Add(new SqlParameter("@ngay_phat_hanh", args[8]));
                cmd.Parameters.Add(new SqlParameter("@anh", image));

                if (cmd.ExecuteNonQuery() > 0)
                {
                    return true;
                }
                return false;
            }
        }

        public bool Delete(string id)
        {
            using (SqlCommand cmd = new SqlCommand("DienThoai_Delete", this.Cnn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@id", int.Parse(id)));

                if (cmd.ExecuteNonQuery() > 0)
                {
                    return true;
                }
                return false;
            }
        }
    }
}
