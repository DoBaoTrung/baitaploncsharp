﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;

namespace QuanLyCuaHangDienThoai.Model
{
    public class Model
    {
        protected static readonly string ConnectionStr = ConfigurationManager.ConnectionStrings["QuanLyCuaHangDienThoai.Properties.Settings.QuanLyCuaHangBanDienThoaiConnectionString"].ConnectionString;

        protected SqlConnection Cnn;

        public Model()
        {
            Cnn = new SqlConnection(ConnectionStr);
            Cnn.Open();
            
        }

        public DataTable GetDataTableByQuery(string query)
        {
            using (SqlCommand cmd = new SqlCommand(query, this.Cnn))
            {
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
        }

        public SqlDataReader GetQueryResult(string query)
        {
            using (SqlCommand cmd = new SqlCommand(query, this.Cnn))
            {
                cmd.CommandType = CommandType.Text;
                SqlDataReader result = cmd.ExecuteReader();
                return result;
            }
        }

        public bool ExecuteNonQuery(string query)
        {
            using (SqlCommand cmd = new SqlCommand(query, this.Cnn))
            {
                cmd.CommandType = CommandType.Text;
                int affectedRows = cmd.ExecuteNonQuery();
                if (affectedRows > 0)
                {
                    return true;
                }
                return false;
            }
        }
    }
}