﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyCuaHangDienThoai.Model
{
    class ChucVu : Model
    {
        public DataTable GetChucVuDataTable()
        {
            return this.GetDataTableByQuery("SELECT * FROM ChucVu");
        }
    }
}
