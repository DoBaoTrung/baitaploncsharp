﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyCuaHangDienThoai.Model
{
    class ChiTietDonHang : Model
    {
        public DataTable GetChiTietDonHangDataTableById(string donHangId)
        {
            return this.GetDataTableByQuery("EXEC ChiTietDonHang_GetByDonHangId @id='" + donHangId + "'");
        }

        public bool Insert(object[] args)
        {
            using (SqlCommand cmd = new SqlCommand("ChiTietDonHang_Insert", this.Cnn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@don_hang_id", args[0]));
                cmd.Parameters.Add(new SqlParameter("@dien_thoai_id", args[1]));
                cmd.Parameters.Add(new SqlParameter("@so_luong", args[2]));

                if (cmd.ExecuteNonQuery() > 0)
                {
                    return true;
                }
                return false;
            }
        }

        public bool Delete(string donHangId)
        {
            using (SqlCommand cmd = new SqlCommand("ChiTietDonHang_Delete", this.Cnn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@don_hang_id", donHangId));

                if (cmd.ExecuteNonQuery() > 0)
                {
                    return true;
                }
                return false;
            }
        }
    }
}