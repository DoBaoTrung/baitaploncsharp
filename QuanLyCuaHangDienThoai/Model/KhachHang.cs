﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyCuaHangDienThoai.Model
{
    public class KhachHang : Model
    {
        public string Id;
        public string HoTen;
        public DataTable GetKhachHangDataTable()
        {
            return this.GetDataTableByQuery("SELECT * FROM View_KhachHang");
        }
        public DataTable GetKetQuaTimKiemDataTable(string searchString)
        {
            string query = "EXEC KhachHang_Search @s = N'" + searchString + "'";
            return this.GetDataTableByQuery(query);
        }
        public SqlDataReader GetById(string id)
        {
            string query = "SELECT * FROM View_KhachHang WHERE View_KhachHang.ID = " + id;
            return this.GetQueryResult(query);
        }
        public KhachHang Forge(string id)
        {
            SqlDataReader result = GetQueryResult("SELECT * FROM KhachHang WHERE id = " + id);
            result.Read();

            this.Id = result[0].ToString();
            this.HoTen = result[2].ToString();
            return this;
        }

        public bool Update(string[] args)
        {
            using (SqlCommand cmd = new SqlCommand("KhachHang_Update", this.Cnn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@id", int.Parse(args[0])));
                cmd.Parameters.Add(new SqlParameter("@ho_ten", args[1]));
                cmd.Parameters.Add(new SqlParameter("@dien_thoai", args[2]));
                cmd.Parameters.Add(new SqlParameter("@email", args[3]));
                cmd.Parameters.Add(new SqlParameter("@dia_chi", args[4]));
                cmd.Parameters.Add(new SqlParameter("@gioi_tinh", int.Parse(args[5])));
             
                if (cmd.ExecuteNonQuery() > 0)
                {
                    return true;
                }
                return false;
            }
           
        }
        public bool Insert(string[] args)
        {
            using (SqlCommand cmd = new SqlCommand("KhachHang_Insert", this.Cnn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ho_ten", args[1]));
                cmd.Parameters.Add(new SqlParameter("@dien_thoai", args[2]));
                cmd.Parameters.Add(new SqlParameter("@email", args[3]));
                cmd.Parameters.Add(new SqlParameter("@dia_chi", args[4]));
                cmd.Parameters.Add(new SqlParameter("@gioi_tinh", args[5]));

                if (cmd.ExecuteNonQuery() > 0)
                {
                    return true;
                }
                return false;
            }
        }
        public bool Delete(string id)
        {
            using (SqlCommand cmd = new SqlCommand("KhachHang_Delete", this.Cnn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@id", int.Parse(id)));

                if (cmd.ExecuteNonQuery() > 0)
                {
                    return true;
                }
                return false;
            }
        }
    }
}