﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyCuaHangDienThoai.Model
{
    class MauSac : Model
    {
        public DataTable GetHangDataTable()
        {
            return this.GetDataTableByQuery("SELECT * FROM MauSac");
        }
    }
}
