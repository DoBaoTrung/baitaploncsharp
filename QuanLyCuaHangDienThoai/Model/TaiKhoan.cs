﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyCuaHangDienThoai.Model
{
    class TaiKhoan: Model
    {
        public static string MaHoa(string text)
        {
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(text));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }
        public string Login(string username, string password)
        {
            password = MaHoa(password);
            string query = "EXEC TaiKhoan_Login @username_or_email = '" + username + "', @password = '" + password + "'";
            SqlDataReader result = this.GetQueryResult(query);
            if (result.HasRows)
            {
                result.Read();
                return result[0].ToString();
            }
            return "";
        }
    }
}