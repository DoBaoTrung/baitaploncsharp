﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace QuanLyCuaHangDienThoai.Pages.KhoHang
{
    public partial class RpSanPham : Form
    {
        public RpSanPham()
        {
            InitializeComponent();
        }

        private void CRV_Load(object sender, EventArgs e)
        {

            // Loc du lieu
            ReportDocument doc = new ReportDocument();
            doc.Load("C:\\Users\\Admin\\source\\repos\\QuanLyCuaHangDienThoai\\QuanLyCuaHangDienThoai\\Reports\\RpSanPham.rpt");
            // doc.RecordSelectionFormula = "{DienThoai.so_luong} > 5";
            this.CRV.ReportSource = doc;
            this.CRV.Refresh();

            // Them parameter
            ParameterFieldDefinition field = doc.DataDefinition.ParameterFields["nguoilap"];
            ParameterValues values = new ParameterValues();
            ParameterDiscreteValue singleValue = new ParameterDiscreteValue();
            singleValue.Value = MasterLayout.GetInstance().LoggedInNhanVien.HoTen;
            values.Add(singleValue);
            field.CurrentValues.Clear();
            field.ApplyCurrentValues(values);
        }
    }
}
