﻿namespace QuanLyCuaHangDienThoai.Pages.KhoHang
{
    partial class QuanLyKho
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NumericBox_Gia = new System.Windows.Forms.NumericUpDown();
            this.TextBox_TimKiem = new System.Windows.Forms.TextBox();
            this.Button_CapNhat = new System.Windows.Forms.Button();
            this.Button_Xoa = new System.Windows.Forms.Button();
            this.Button_Them = new System.Windows.Forms.Button();
            this.ComboBox_Mau = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.DateTimePicker_NgayPH = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.TextBox_MaSKU = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TextBox_TenMay = new System.Windows.Forms.TextBox();
            this.TextBox_Id = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DataGridView_DienThoai = new System.Windows.Forms.DataGridView();
            this.NumericBox_SoLuong = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.RichTextBox_ThongSo = new System.Windows.Forms.RichTextBox();
            this.ComboBox_Hang = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.Button_DangAnh = new System.Windows.Forms.Button();
            this.PictureBox_DienThoai = new System.Windows.Forms.PictureBox();
            this.Button_TimKiem = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.NumericBox_Gia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_DienThoai)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericBox_SoLuong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_DienThoai)).BeginInit();
            this.SuspendLayout();
            // 
            // NumericBox_Gia
            // 
            this.NumericBox_Gia.Increment = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.NumericBox_Gia.Location = new System.Drawing.Point(66, 382);
            this.NumericBox_Gia.Maximum = new decimal(new int[] {
            1410065408,
            2,
            0,
            0});
            this.NumericBox_Gia.Name = "NumericBox_Gia";
            this.NumericBox_Gia.Size = new System.Drawing.Size(181, 20);
            this.NumericBox_Gia.TabIndex = 50;
            this.NumericBox_Gia.ThousandsSeparator = true;
            // 
            // TextBox_TimKiem
            // 
            this.TextBox_TimKiem.Location = new System.Drawing.Point(601, 12);
            this.TextBox_TimKiem.Name = "TextBox_TimKiem";
            this.TextBox_TimKiem.Size = new System.Drawing.Size(164, 20);
            this.TextBox_TimKiem.TabIndex = 48;
            // 
            // Button_CapNhat
            // 
            this.Button_CapNhat.Enabled = false;
            this.Button_CapNhat.Location = new System.Drawing.Point(585, 382);
            this.Button_CapNhat.Name = "Button_CapNhat";
            this.Button_CapNhat.Size = new System.Drawing.Size(64, 23);
            this.Button_CapNhat.TabIndex = 47;
            this.Button_CapNhat.Text = "Cập nhật";
            this.Button_CapNhat.UseVisualStyleBackColor = true;
            this.Button_CapNhat.Click += new System.EventHandler(this.Button_CapNhat_Click);
            // 
            // Button_Xoa
            // 
            this.Button_Xoa.BackColor = System.Drawing.Color.Crimson;
            this.Button_Xoa.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.Button_Xoa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_Xoa.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Button_Xoa.Location = new System.Drawing.Point(732, 384);
            this.Button_Xoa.Margin = new System.Windows.Forms.Padding(0);
            this.Button_Xoa.Name = "Button_Xoa";
            this.Button_Xoa.Size = new System.Drawing.Size(57, 21);
            this.Button_Xoa.TabIndex = 46;
            this.Button_Xoa.Text = "Xóa";
            this.Button_Xoa.UseVisualStyleBackColor = false;
            this.Button_Xoa.Click += new System.EventHandler(this.Button_Xoa_Click);
            // 
            // Button_Them
            // 
            this.Button_Them.Location = new System.Drawing.Point(661, 383);
            this.Button_Them.Name = "Button_Them";
            this.Button_Them.Size = new System.Drawing.Size(55, 23);
            this.Button_Them.TabIndex = 45;
            this.Button_Them.Text = "Thêm";
            this.Button_Them.UseVisualStyleBackColor = true;
            this.Button_Them.Click += new System.EventHandler(this.Button_Them_Click);
            // 
            // ComboBox_Mau
            // 
            this.ComboBox_Mau.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ComboBox_Mau.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ComboBox_Mau.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBox_Mau.FormattingEnabled = true;
            this.ComboBox_Mau.Location = new System.Drawing.Point(322, 324);
            this.ComboBox_Mau.Name = "ComboBox_Mau";
            this.ComboBox_Mau.Size = new System.Drawing.Size(180, 21);
            this.ComboBox_Mau.TabIndex = 44;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(286, 326);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(28, 13);
            this.label8.TabIndex = 43;
            this.label8.Text = "Màu";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(36, 384);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 13);
            this.label7.TabIndex = 42;
            this.label7.Text = "Giá";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(525, 298);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 40;
            this.label6.Text = "Thông số";
            // 
            // DateTimePicker_NgayPH
            // 
            this.DateTimePicker_NgayPH.CustomFormat = "dd/MM/yyyy";
            this.DateTimePicker_NgayPH.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DateTimePicker_NgayPH.Location = new System.Drawing.Point(322, 354);
            this.DateTimePicker_NgayPH.Name = "DateTimePicker_NgayPH";
            this.DateTimePicker_NgayPH.Size = new System.Drawing.Size(180, 20);
            this.DateTimePicker_NgayPH.TabIndex = 37;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(264, 356);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 36;
            this.label4.Text = "Ngày PH";
            // 
            // TextBox_MaSKU
            // 
            this.TextBox_MaSKU.Location = new System.Drawing.Point(66, 323);
            this.TextBox_MaSKU.Name = "TextBox_MaSKU";
            this.TextBox_MaSKU.Size = new System.Drawing.Size(181, 20);
            this.TextBox_MaSKU.TabIndex = 35;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 326);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 34;
            this.label3.Text = "SKU";
            // 
            // TextBox_TenMay
            // 
            this.TextBox_TenMay.Location = new System.Drawing.Point(66, 353);
            this.TextBox_TenMay.Name = "TextBox_TenMay";
            this.TextBox_TenMay.Size = new System.Drawing.Size(181, 20);
            this.TextBox_TenMay.TabIndex = 33;
            // 
            // TextBox_Id
            // 
            this.TextBox_Id.Location = new System.Drawing.Point(66, 293);
            this.TextBox_Id.Name = "TextBox_Id";
            this.TextBox_Id.ReadOnly = true;
            this.TextBox_Id.Size = new System.Drawing.Size(93, 20);
            this.TextBox_Id.TabIndex = 32;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(40, 296);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 13);
            this.label2.TabIndex = 31;
            this.label2.Text = "ID";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 356);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 30;
            this.label1.Text = "Tên máy";
            // 
            // DataGridView_DienThoai
            // 
            this.DataGridView_DienThoai.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView_DienThoai.Location = new System.Drawing.Point(12, 12);
            this.DataGridView_DienThoai.Name = "DataGridView_DienThoai";
            this.DataGridView_DienThoai.ReadOnly = true;
            this.DataGridView_DienThoai.Size = new System.Drawing.Size(573, 245);
            this.DataGridView_DienThoai.TabIndex = 29;
            this.DataGridView_DienThoai.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView_DienThoai_CellClick);
            this.DataGridView_DienThoai.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView_DienThoai_CellContentClick);
            // 
            // NumericBox_SoLuong
            // 
            this.NumericBox_SoLuong.Location = new System.Drawing.Point(322, 382);
            this.NumericBox_SoLuong.Name = "NumericBox_SoLuong";
            this.NumericBox_SoLuong.Size = new System.Drawing.Size(180, 20);
            this.NumericBox_SoLuong.TabIndex = 52;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(268, 384);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 51;
            this.label9.Text = "Số lượng";
            // 
            // RichTextBox_ThongSo
            // 
            this.RichTextBox_ThongSo.Location = new System.Drawing.Point(583, 294);
            this.RichTextBox_ThongSo.Name = "RichTextBox_ThongSo";
            this.RichTextBox_ThongSo.Size = new System.Drawing.Size(205, 74);
            this.RichTextBox_ThongSo.TabIndex = 53;
            this.RichTextBox_ThongSo.Text = "";
            // 
            // ComboBox_Hang
            // 
            this.ComboBox_Hang.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ComboBox_Hang.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ComboBox_Hang.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBox_Hang.FormattingEnabled = true;
            this.ComboBox_Hang.Location = new System.Drawing.Point(322, 295);
            this.ComboBox_Hang.Name = "ComboBox_Hang";
            this.ComboBox_Hang.Size = new System.Drawing.Size(180, 21);
            this.ComboBox_Hang.TabIndex = 56;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(282, 298);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(33, 13);
            this.label10.TabIndex = 55;
            this.label10.Text = "Hãng";
            // 
            // Button_DangAnh
            // 
            this.Button_DangAnh.Location = new System.Drawing.Point(165, 292);
            this.Button_DangAnh.Name = "Button_DangAnh";
            this.Button_DangAnh.Size = new System.Drawing.Size(82, 23);
            this.Button_DangAnh.TabIndex = 57;
            this.Button_DangAnh.Text = "Đăng ảnh";
            this.Button_DangAnh.UseVisualStyleBackColor = true;
            this.Button_DangAnh.Click += new System.EventHandler(this.Button_DangAnh_Click);
            // 
            // PictureBox_DienThoai
            // 
            this.PictureBox_DienThoai.Image = global::QuanLyCuaHangDienThoai.Properties.Resources.placeholder;
            this.PictureBox_DienThoai.InitialImage = global::QuanLyCuaHangDienThoai.Properties.Resources.placeholder;
            this.PictureBox_DienThoai.Location = new System.Drawing.Point(601, 48);
            this.PictureBox_DienThoai.Name = "PictureBox_DienThoai";
            this.PictureBox_DienThoai.Size = new System.Drawing.Size(188, 209);
            this.PictureBox_DienThoai.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBox_DienThoai.TabIndex = 54;
            this.PictureBox_DienThoai.TabStop = false;
            // 
            // Button_TimKiem
            // 
            this.Button_TimKiem.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Button_TimKiem.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.Button_TimKiem.FlatAppearance.MouseDownBackColor = System.Drawing.Color.RoyalBlue;
            this.Button_TimKiem.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.Button_TimKiem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_TimKiem.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_TimKiem.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.Button_TimKiem.Image = global::QuanLyCuaHangDienThoai.Properties.Resources.icon_search_white;
            this.Button_TimKiem.Location = new System.Drawing.Point(761, 12);
            this.Button_TimKiem.Name = "Button_TimKiem";
            this.Button_TimKiem.Size = new System.Drawing.Size(27, 20);
            this.Button_TimKiem.TabIndex = 49;
            this.Button_TimKiem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Button_TimKiem.UseVisualStyleBackColor = false;
            this.Button_TimKiem.Click += new System.EventHandler(this.Button_TimKiem_Click);
            // 
            // QuanLyKho
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Button_DangAnh);
            this.Controls.Add(this.ComboBox_Hang);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.PictureBox_DienThoai);
            this.Controls.Add(this.RichTextBox_ThongSo);
            this.Controls.Add(this.NumericBox_SoLuong);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.NumericBox_Gia);
            this.Controls.Add(this.Button_TimKiem);
            this.Controls.Add(this.TextBox_TimKiem);
            this.Controls.Add(this.Button_CapNhat);
            this.Controls.Add(this.Button_Xoa);
            this.Controls.Add(this.Button_Them);
            this.Controls.Add(this.ComboBox_Mau);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.DateTimePicker_NgayPH);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TextBox_MaSKU);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TextBox_TenMay);
            this.Controls.Add(this.TextBox_Id);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DataGridView_DienThoai);
            this.Name = "QuanLyKho";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.QuanLyKho_Load);
            ((System.ComponentModel.ISupportInitialize)(this.NumericBox_Gia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_DienThoai)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericBox_SoLuong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_DienThoai)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown NumericBox_Gia;
        private System.Windows.Forms.Button Button_TimKiem;
        private System.Windows.Forms.TextBox TextBox_TimKiem;
        private System.Windows.Forms.Button Button_CapNhat;
        private System.Windows.Forms.Button Button_Xoa;
        private System.Windows.Forms.Button Button_Them;
        private System.Windows.Forms.ComboBox ComboBox_Mau;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker DateTimePicker_NgayPH;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TextBox_MaSKU;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TextBox_TenMay;
        private System.Windows.Forms.TextBox TextBox_Id;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView DataGridView_DienThoai;
        private System.Windows.Forms.NumericUpDown NumericBox_SoLuong;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RichTextBox RichTextBox_ThongSo;
        private System.Windows.Forms.PictureBox PictureBox_DienThoai;
        private System.Windows.Forms.ComboBox ComboBox_Hang;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button Button_DangAnh;
    }
}