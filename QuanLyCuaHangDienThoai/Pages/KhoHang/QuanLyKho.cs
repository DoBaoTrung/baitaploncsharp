﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyCuaHangDienThoai.Pages.KhoHang
{
    public partial class QuanLyKho : Form
    {
        public QuanLyKho()
        {
            InitializeComponent();
        }

        private void QuanLyKho_Load(object sender, EventArgs e)
        {
            
            Model.Hang hang = new Model.Hang();
            this.ComboBox_Hang.DataSource = hang.GetHangDataTable();
            this.ComboBox_Hang.DisplayMember = "ten";
            this.ComboBox_Hang.ValueMember = "id";

            Model.MauSac mauSac = new Model.MauSac();
            this.ComboBox_Mau.DataSource = mauSac.GetHangDataTable();
            this.ComboBox_Mau.DisplayMember = "ten_mau";
            this.ComboBox_Mau.ValueMember = "id";

            this.FetchDienThoaiDataTable();
        }

        private void FetchDienThoaiDataTable()
        {
            Model.DienThoai dt = new Model.DienThoai();
            this.DataGridView_DienThoai.DataSource = dt.GetDienThoaiDataTable();
        }

        private void DataGridView_DienThoai_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1) return; // Tranh nguoi dung an vao tieu de bang

            DataGridViewRow row = DataGridView_DienThoai.Rows[e.RowIndex];
            string id = row.Cells[0].Value.ToString();
            string maSKU = row.Cells[1].Value.ToString();
            string ten = row.Cells[2].Value.ToString();
            string gia = row.Cells[3].Value.ToString();
            string soLuong = row.Cells[4].Value.ToString();
            string thongSo = row.Cells[5].Value.ToString();
            string mauSac = row.Cells[6].Value.ToString();
            string hang = row.Cells[7].Value.ToString();
            string ngayPhatHanh = row.Cells[8].Value.ToString();

            Model.DienThoai dt = new Model.DienThoai();

            if (id == "")
            {
                this.Button_Them.Enabled = true;
                this.Button_CapNhat.Enabled = false;
                this.PictureBox_DienThoai.Image = this.PictureBox_DienThoai.InitialImage;
            }
            else
            {
                this.Button_Them.Enabled = false;
                this.Button_CapNhat.Enabled = true;
                this.PictureBox_DienThoai.Image = dt.GetImageById(id);
            }
            this.TextBox_Id.Text = id;
            this.TextBox_MaSKU.Text = maSKU;
            this.TextBox_TenMay.Text = ten;
            this.NumericBox_Gia.Value = int.Parse(gia == "" ? "0" : gia);
            this.NumericBox_SoLuong.Value = int.Parse(soLuong == "" ? "0" : soLuong);
            this.RichTextBox_ThongSo.Text = thongSo;
            this.ComboBox_Mau.SelectedIndex = this.ComboBox_Mau.FindStringExact(mauSac);
            this.ComboBox_Hang.SelectedIndex = this.ComboBox_Hang.FindStringExact(hang);

            if (!ngayPhatHanh.Equals(""))
            {
                this.DateTimePicker_NgayPH.Value = Convert.ToDateTime(ngayPhatHanh);
            }
        }

        private object[] FetchAllFormData()
        {
            object[] args = new object[10];
            args[0] = this.TextBox_Id.Text;
            args[1] = this.TextBox_MaSKU.Text;
            args[2] = this.TextBox_TenMay.Text;
            args[3] = this.NumericBox_Gia.Value.ToString();
            args[4] = this.NumericBox_SoLuong.Value.ToString();
            args[5] = this.RichTextBox_ThongSo.Text;
            args[6] = this.ComboBox_Mau.SelectedValue.ToString();
            args[7] = this.ComboBox_Hang.SelectedValue.ToString();
            args[8] = this.DateTimePicker_NgayPH.Value;
            return args;
        }

        private byte[] GetImageData()
        {
            MemoryStream stream = new MemoryStream();
            PictureBox_DienThoai.Image.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
            byte[] pic = stream.ToArray();
            return pic;
        }

        private void Button_DangAnh_Click(object sender, EventArgs e)
        {
            OpenFileDialog opnfd = new OpenFileDialog();
            opnfd.Filter = "Image Files (*.jpg;*.jpeg;.*.gif;)|*.jpg;*.jpeg;.*.gif";
            if (opnfd.ShowDialog() == DialogResult.OK)
            {
                PictureBox_DienThoai.Image = new Bitmap(opnfd.FileName);
            }
        }

        private void Button_CapNhat_Click(object sender, EventArgs e)
        {
            Model.DienThoai dt = new Model.DienThoai();
            try
            {
                bool result = dt.Update(FetchAllFormData(), GetImageData());

                if (result)
                {
                    MessageBox.Show("Đã Lưu.");
                    FetchDienThoaiDataTable();
                }
            }
            catch (Exception bug)
            {

                MessageBox.Show(bug.ToString());
            }
        }

        private void Button_Them_Click(object sender, EventArgs e)
        {
            Model.DienThoai dt = new Model.DienThoai();

            try
            {
                bool result = dt.Insert(FetchAllFormData(), GetImageData());

                if (result)
                {
                    MessageBox.Show("Đã thêm.");
                    FetchDienThoaiDataTable();
                }
            }
            catch (Exception bug)
            {

                MessageBox.Show(bug.ToString());
            }
        }

        private void Button_Xoa_Click(object sender, EventArgs e)
        {
            Model.DienThoai dt = new Model.DienThoai();

            try
            {
                bool result = dt.Delete(this.TextBox_Id.Text);

                if (result)
                {
                    MessageBox.Show("Đã Xóa.");
                    FetchDienThoaiDataTable();
                }
            }
            catch (Exception bug)
            {

                MessageBox.Show(bug.ToString());
            }
        }

        private void Button_TimKiem_Click(object sender, EventArgs e)
        {
            Model.DienThoai dt = new Model.DienThoai();

            DataTable result = dt.GetKetQuaTimKiemDataTable(this.TextBox_TimKiem.Text);
            this.DataGridView_DienThoai.DataSource = result;
        }

        private void DataGridView_DienThoai_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }
    }
}