﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyCuaHangDienThoai.Pages
{
    public partial class Login : Form
    {
        private static Login Instance;

        private Login()
        {
            InitializeComponent();
        }

        public static Login GetInstance()
        {
            if (Instance == null)
            {
                Instance = new Login();
            }
            return Instance;
        }

        private void TextBox_TenDangNhap_Changed(object sender, EventArgs e)
        {

        }

        private void Button_DangNhap_Click(object sender, EventArgs e)
        {
            string tenDangNhap = TextBox_TenDangNhap.Text;
            string matKhau = TextBox_MatKhau.Text;

            var taiKhoan = new Model.TaiKhoan();
            string idNhanVien = taiKhoan.Login(tenDangNhap, matKhau);

            if (idNhanVien != "")
            {
                this.Hide();
                MasterLayout.GetInstance().Show();
                MasterLayout.GetInstance().AddLoggedInNhanVien(idNhanVien);
            }
            else
            {
                MessageBox.Show("Sai tên đăng nhập hoặc mật khẩu.");
            }
        }
    }
}