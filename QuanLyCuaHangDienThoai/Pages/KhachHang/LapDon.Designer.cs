﻿namespace QuanLyCuaHangDienThoai.Pages.KhachHang
{
    partial class LapDon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PictureBox_DienThoai = new System.Windows.Forms.PictureBox();
            this.DataGridView_DonHang = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.ComboBox_DienThoai = new System.Windows.Forms.ComboBox();
            this.NumericBox_SoLuong = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.Button_ThemVaoDon = new System.Windows.Forms.Button();
            this.Button_LuuDonHang = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.RichTextBox_GhiChu = new System.Windows.Forms.RichTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.Label_KhachHang = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Label_TongGiaTri = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Label_NguoiLap = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ComboBox_TrangThai = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.NumericBox_TienKhachDua = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_DienThoai)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_DonHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericBox_SoLuong)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericBox_TienKhachDua)).BeginInit();
            this.SuspendLayout();
            // 
            // PictureBox_DienThoai
            // 
            this.PictureBox_DienThoai.Image = global::QuanLyCuaHangDienThoai.Properties.Resources.placeholder;
            this.PictureBox_DienThoai.InitialImage = global::QuanLyCuaHangDienThoai.Properties.Resources.placeholder;
            this.PictureBox_DienThoai.Location = new System.Drawing.Point(600, 12);
            this.PictureBox_DienThoai.Name = "PictureBox_DienThoai";
            this.PictureBox_DienThoai.Size = new System.Drawing.Size(188, 193);
            this.PictureBox_DienThoai.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBox_DienThoai.TabIndex = 56;
            this.PictureBox_DienThoai.TabStop = false;
            // 
            // DataGridView_DonHang
            // 
            this.DataGridView_DonHang.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView_DonHang.Location = new System.Drawing.Point(12, 12);
            this.DataGridView_DonHang.Name = "DataGridView_DonHang";
            this.DataGridView_DonHang.ReadOnly = true;
            this.DataGridView_DonHang.Size = new System.Drawing.Size(572, 193);
            this.DataGridView_DonHang.TabIndex = 55;
            this.DataGridView_DonHang.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView_DonHang_CellClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 236);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 57;
            this.label1.Text = "Điện thoại";
            // 
            // ComboBox_DienThoai
            // 
            this.ComboBox_DienThoai.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ComboBox_DienThoai.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ComboBox_DienThoai.FormattingEnabled = true;
            this.ComboBox_DienThoai.Location = new System.Drawing.Point(75, 233);
            this.ComboBox_DienThoai.Name = "ComboBox_DienThoai";
            this.ComboBox_DienThoai.Size = new System.Drawing.Size(188, 21);
            this.ComboBox_DienThoai.TabIndex = 58;
            // 
            // NumericBox_SoLuong
            // 
            this.NumericBox_SoLuong.Location = new System.Drawing.Point(75, 266);
            this.NumericBox_SoLuong.Name = "NumericBox_SoLuong";
            this.NumericBox_SoLuong.Size = new System.Drawing.Size(188, 20);
            this.NumericBox_SoLuong.TabIndex = 60;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(21, 268);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 59;
            this.label9.Text = "Số lượng";
            // 
            // Button_ThemVaoDon
            // 
            this.Button_ThemVaoDon.Location = new System.Drawing.Point(75, 298);
            this.Button_ThemVaoDon.Name = "Button_ThemVaoDon";
            this.Button_ThemVaoDon.Size = new System.Drawing.Size(134, 23);
            this.Button_ThemVaoDon.TabIndex = 61;
            this.Button_ThemVaoDon.Text = "Thêm vào đơn hàng";
            this.Button_ThemVaoDon.UseVisualStyleBackColor = true;
            this.Button_ThemVaoDon.Click += new System.EventHandler(this.Button_ThemVaoDon_Click);
            // 
            // Button_LuuDonHang
            // 
            this.Button_LuuDonHang.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.Button_LuuDonHang.FlatAppearance.BorderColor = System.Drawing.Color.LightSeaGreen;
            this.Button_LuuDonHang.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_LuuDonHang.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_LuuDonHang.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.Button_LuuDonHang.Location = new System.Drawing.Point(85, 165);
            this.Button_LuuDonHang.Name = "Button_LuuDonHang";
            this.Button_LuuDonHang.Size = new System.Drawing.Size(134, 28);
            this.Button_LuuDonHang.TabIndex = 62;
            this.Button_LuuDonHang.Text = "Lưu đơn hàng";
            this.Button_LuuDonHang.UseVisualStyleBackColor = false;
            this.Button_LuuDonHang.Click += new System.EventHandler(this.Button_LuuDonHang_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.RichTextBox_GhiChu);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.Label_KhachHang);
            this.groupBox1.Controls.Add(this.Button_LuuDonHang);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.Label_TongGiaTri);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.Label_NguoiLap);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(380, 218);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(395, 212);
            this.groupBox1.TabIndex = 63;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin hóa đơn";
            // 
            // RichTextBox_GhiChu
            // 
            this.RichTextBox_GhiChu.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RichTextBox_GhiChu.Location = new System.Drawing.Point(85, 98);
            this.RichTextBox_GhiChu.Name = "RichTextBox_GhiChu";
            this.RichTextBox_GhiChu.Size = new System.Drawing.Size(287, 56);
            this.RichTextBox_GhiChu.TabIndex = 64;
            this.RichTextBox_GhiChu.Text = "";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(31, 98);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 13);
            this.label11.TabIndex = 63;
            this.label11.Text = "Ghi chú:";
            // 
            // Label_KhachHang
            // 
            this.Label_KhachHang.AutoSize = true;
            this.Label_KhachHang.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_KhachHang.Location = new System.Drawing.Point(84, 50);
            this.Label_KhachHang.Name = "Label_KhachHang";
            this.Label_KhachHang.Size = new System.Drawing.Size(86, 13);
            this.Label_KhachHang.TabIndex = 5;
            this.Label_KhachHang.Text = "(label tên khách)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(10, 50);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Khách hàng:";
            // 
            // Label_TongGiaTri
            // 
            this.Label_TongGiaTri.AutoSize = true;
            this.Label_TongGiaTri.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_TongGiaTri.Location = new System.Drawing.Point(84, 74);
            this.Label_TongGiaTri.Name = "Label_TongGiaTri";
            this.Label_TongGiaTri.Size = new System.Drawing.Size(13, 13);
            this.Label_TongGiaTri.TabIndex = 3;
            this.Label_TongGiaTri.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(15, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Tổng giá trị:";
            // 
            // Label_NguoiLap
            // 
            this.Label_NguoiLap.AutoSize = true;
            this.Label_NguoiLap.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_NguoiLap.Location = new System.Drawing.Point(84, 26);
            this.Label_NguoiLap.Name = "Label_NguoiLap";
            this.Label_NguoiLap.Size = new System.Drawing.Size(81, 13);
            this.Label_NguoiLap.TabIndex = 1;
            this.Label_NguoiLap.Text = "(label nguoi lap)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(23, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Người lập:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 367);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 64;
            this.label4.Text = "Trạng thái";
            // 
            // ComboBox_TrangThai
            // 
            this.ComboBox_TrangThai.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ComboBox_TrangThai.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ComboBox_TrangThai.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBox_TrangThai.FormattingEnabled = true;
            this.ComboBox_TrangThai.Location = new System.Drawing.Point(75, 370);
            this.ComboBox_TrangThai.Name = "ComboBox_TrangThai";
            this.ComboBox_TrangThai.Size = new System.Drawing.Size(188, 21);
            this.ComboBox_TrangThai.TabIndex = 65;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(43, 380);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(26, 13);
            this.label5.TabIndex = 66;
            this.label5.Text = "đơn";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 332);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 13);
            this.label7.TabIndex = 67;
            this.label7.Text = "Tiền khách";
            // 
            // NumericBox_TienKhachDua
            // 
            this.NumericBox_TienKhachDua.Increment = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.NumericBox_TienKhachDua.Location = new System.Drawing.Point(75, 334);
            this.NumericBox_TienKhachDua.Maximum = new decimal(new int[] {
            -727379968,
            232,
            0,
            0});
            this.NumericBox_TienKhachDua.Name = "NumericBox_TienKhachDua";
            this.NumericBox_TienKhachDua.Size = new System.Drawing.Size(188, 20);
            this.NumericBox_TienKhachDua.TabIndex = 68;
            this.NumericBox_TienKhachDua.ThousandsSeparator = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(43, 345);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(26, 13);
            this.label8.TabIndex = 69;
            this.label8.Text = "đưa";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(269, 338);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(30, 13);
            this.label10.TabIndex = 70;
            this.label10.Text = "VND";
            // 
            // LapDon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.NumericBox_TienKhachDua);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ComboBox_TrangThai);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Button_ThemVaoDon);
            this.Controls.Add(this.NumericBox_SoLuong);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.ComboBox_DienThoai);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PictureBox_DienThoai);
            this.Controls.Add(this.DataGridView_DonHang);
            this.Name = "LapDon";
            this.Text = "Lập đơn hàng";
            this.Load += new System.EventHandler(this.LapDon_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_DienThoai)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_DonHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericBox_SoLuong)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericBox_TienKhachDua)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox PictureBox_DienThoai;
        private System.Windows.Forms.DataGridView DataGridView_DonHang;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ComboBox_DienThoai;
        private System.Windows.Forms.NumericUpDown NumericBox_SoLuong;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button Button_ThemVaoDon;
        private System.Windows.Forms.Button Button_LuuDonHang;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label Label_KhachHang;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label Label_TongGiaTri;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label Label_NguoiLap;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox RichTextBox_GhiChu;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox ComboBox_TrangThai;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown NumericBox_TienKhachDua;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
    }
}