﻿namespace QuanLyCuaHangDienThoai.Pages.KhachHang
{
    partial class DonHang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Button_TimKiem = new System.Windows.Forms.Button();
            this.TextBox_TimKiem = new System.Windows.Forms.TextBox();
            this.DataGridView_DonHang = new System.Windows.Forms.DataGridView();
            this.Button_XemDonHang = new System.Windows.Forms.Button();
            this.Button_Xoa = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_DonHang)).BeginInit();
            this.SuspendLayout();
            // 
            // Button_TimKiem
            // 
            this.Button_TimKiem.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Button_TimKiem.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.Button_TimKiem.FlatAppearance.MouseDownBackColor = System.Drawing.Color.RoyalBlue;
            this.Button_TimKiem.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.Button_TimKiem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_TimKiem.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_TimKiem.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.Button_TimKiem.Image = global::QuanLyCuaHangDienThoai.Properties.Resources.icon_search_white;
            this.Button_TimKiem.Location = new System.Drawing.Point(494, 229);
            this.Button_TimKiem.Name = "Button_TimKiem";
            this.Button_TimKiem.Size = new System.Drawing.Size(23, 20);
            this.Button_TimKiem.TabIndex = 30;
            this.Button_TimKiem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Button_TimKiem.UseVisualStyleBackColor = false;
            this.Button_TimKiem.Click += new System.EventHandler(this.Button_TimKiem_Click);
            // 
            // TextBox_TimKiem
            // 
            this.TextBox_TimKiem.Location = new System.Drawing.Point(256, 229);
            this.TextBox_TimKiem.Name = "TextBox_TimKiem";
            this.TextBox_TimKiem.Size = new System.Drawing.Size(241, 20);
            this.TextBox_TimKiem.TabIndex = 29;
            // 
            // DataGridView_DonHang
            // 
            this.DataGridView_DonHang.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView_DonHang.Location = new System.Drawing.Point(12, 12);
            this.DataGridView_DonHang.Name = "DataGridView_DonHang";
            this.DataGridView_DonHang.ReadOnly = true;
            this.DataGridView_DonHang.Size = new System.Drawing.Size(776, 199);
            this.DataGridView_DonHang.TabIndex = 28;
            this.DataGridView_DonHang.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView_DonHang_CellClick);
            // 
            // Button_XemDonHang
            // 
            this.Button_XemDonHang.Location = new System.Drawing.Point(331, 263);
            this.Button_XemDonHang.Name = "Button_XemDonHang";
            this.Button_XemDonHang.Size = new System.Drawing.Size(111, 29);
            this.Button_XemDonHang.TabIndex = 33;
            this.Button_XemDonHang.Text = "Xem đơn hàng";
            this.Button_XemDonHang.UseVisualStyleBackColor = true;
            this.Button_XemDonHang.Click += new System.EventHandler(this.Button_XemDonHang_Click);
            // 
            // Button_Xoa
            // 
            this.Button_Xoa.BackColor = System.Drawing.Color.Firebrick;
            this.Button_Xoa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_Xoa.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.Button_Xoa.Location = new System.Drawing.Point(331, 304);
            this.Button_Xoa.Name = "Button_Xoa";
            this.Button_Xoa.Size = new System.Drawing.Size(111, 29);
            this.Button_Xoa.TabIndex = 34;
            this.Button_Xoa.Text = "Xóa đơn hàng";
            this.Button_Xoa.UseVisualStyleBackColor = false;
            this.Button_Xoa.Click += new System.EventHandler(this.Button_Xoa_Click);
            // 
            // DonHang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Button_Xoa);
            this.Controls.Add(this.Button_XemDonHang);
            this.Controls.Add(this.Button_TimKiem);
            this.Controls.Add(this.TextBox_TimKiem);
            this.Controls.Add(this.DataGridView_DonHang);
            this.Name = "DonHang";
            this.Text = "Đơn hàng";
            this.Load += new System.EventHandler(this.DonHang_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_DonHang)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Button_TimKiem;
        private System.Windows.Forms.TextBox TextBox_TimKiem;
        private System.Windows.Forms.DataGridView DataGridView_DonHang;
        private System.Windows.Forms.Button Button_XemDonHang;
        private System.Windows.Forms.Button Button_Xoa;
    }
}