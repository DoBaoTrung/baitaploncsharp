﻿namespace QuanLyCuaHangDienThoai.Pages.KhachHang
{
    partial class QuanlyKH
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.DataGridView_KhachHang = new System.Windows.Forms.DataGridView();
            this.khachHangBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.quanLyCuaHangBanDienThoaiDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.quanLyCuaHangBanDienThoaiDataSet = new QuanLyCuaHangDienThoai.QuanLyCuaHangBanDienThoaiDataSet();
            this.TextBox_HoTen = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.khachHangTableAdapter = new QuanLyCuaHangDienThoai.QuanLyCuaHangBanDienThoaiDataSetTableAdapters.KhachHangTableAdapter();
            this.label2 = new System.Windows.Forms.Label();
            this.TextBox_DienThoai = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TextBox_ID = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TextBox_Email = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.RadioButton_GioiTinh_Nam = new System.Windows.Forms.RadioButton();
            this.RadioButton_GioiTinh_Nu = new System.Windows.Forms.RadioButton();
            this.Panel_GioiTinh = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.TextBox_DiaChi = new System.Windows.Forms.TextBox();
            this.Button_Them = new System.Windows.Forms.Button();
            this.Button_Xoa = new System.Windows.Forms.Button();
            this.Button_CapNhat = new System.Windows.Forms.Button();
            this.TextBox_TimKiem = new System.Windows.Forms.TextBox();
            this.Button_TimKiem = new System.Windows.Forms.Button();
            this.Button_LapDon = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_KhachHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.khachHangBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.quanLyCuaHangBanDienThoaiDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.quanLyCuaHangBanDienThoaiDataSet)).BeginInit();
            this.Panel_GioiTinh.SuspendLayout();
            this.SuspendLayout();
            // 
            // DataGridView_KhachHang
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridView_KhachHang.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridView_KhachHang.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGridView_KhachHang.DefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridView_KhachHang.Location = new System.Drawing.Point(12, 13);
            this.DataGridView_KhachHang.Name = "DataGridView_KhachHang";
            this.DataGridView_KhachHang.ReadOnly = true;
            this.DataGridView_KhachHang.Size = new System.Drawing.Size(776, 211);
            this.DataGridView_KhachHang.TabIndex = 1;
            this.DataGridView_KhachHang.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView_KhachHang_CellClick);
            // 
            // khachHangBindingSource
            // 
            this.khachHangBindingSource.DataMember = "KhachHang";
            this.khachHangBindingSource.DataSource = this.quanLyCuaHangBanDienThoaiDataSetBindingSource;
            // 
            // quanLyCuaHangBanDienThoaiDataSetBindingSource
            // 
            this.quanLyCuaHangBanDienThoaiDataSetBindingSource.DataSource = this.quanLyCuaHangBanDienThoaiDataSet;
            this.quanLyCuaHangBanDienThoaiDataSetBindingSource.Position = 0;
            // 
            // quanLyCuaHangBanDienThoaiDataSet
            // 
            this.quanLyCuaHangBanDienThoaiDataSet.DataSetName = "QuanLyCuaHangBanDienThoaiDataSet";
            this.quanLyCuaHangBanDienThoaiDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // TextBox_HoTen
            // 
            this.TextBox_HoTen.Location = new System.Drawing.Point(93, 299);
            this.TextBox_HoTen.Name = "TextBox_HoTen";
            this.TextBox_HoTen.Size = new System.Drawing.Size(218, 20);
            this.TextBox_HoTen.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 302);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Họ tên";
            // 
            // khachHangTableAdapter
            // 
            this.khachHangTableAdapter.ClearBeforeFill = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 332);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Điện thoại";
            // 
            // TextBox_DienThoai
            // 
            this.TextBox_DienThoai.Location = new System.Drawing.Point(93, 328);
            this.TextBox_DienThoai.Name = "TextBox_DienThoai";
            this.TextBox_DienThoai.Size = new System.Drawing.Size(218, 20);
            this.TextBox_DienThoai.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(66, 273);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "ID";
            // 
            // TextBox_ID
            // 
            this.TextBox_ID.Location = new System.Drawing.Point(93, 270);
            this.TextBox_ID.Name = "TextBox_ID";
            this.TextBox_ID.ReadOnly = true;
            this.TextBox_ID.Size = new System.Drawing.Size(58, 20);
            this.TextBox_ID.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(51, 361);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Email";
            // 
            // TextBox_Email
            // 
            this.TextBox_Email.Location = new System.Drawing.Point(93, 358);
            this.TextBox_Email.Name = "TextBox_Email";
            this.TextBox_Email.Size = new System.Drawing.Size(218, 20);
            this.TextBox_Email.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(36, 388);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Giới tính";
            // 
            // RadioButton_GioiTinh_Nam
            // 
            this.RadioButton_GioiTinh_Nam.AutoSize = true;
            this.RadioButton_GioiTinh_Nam.Location = new System.Drawing.Point(3, 3);
            this.RadioButton_GioiTinh_Nam.Name = "RadioButton_GioiTinh_Nam";
            this.RadioButton_GioiTinh_Nam.Size = new System.Drawing.Size(47, 17);
            this.RadioButton_GioiTinh_Nam.TabIndex = 12;
            this.RadioButton_GioiTinh_Nam.TabStop = true;
            this.RadioButton_GioiTinh_Nam.Tag = "1";
            this.RadioButton_GioiTinh_Nam.Text = "Nam";
            this.RadioButton_GioiTinh_Nam.UseVisualStyleBackColor = true;
            // 
            // RadioButton_GioiTinh_Nu
            // 
            this.RadioButton_GioiTinh_Nu.AutoSize = true;
            this.RadioButton_GioiTinh_Nu.Location = new System.Drawing.Point(56, 3);
            this.RadioButton_GioiTinh_Nu.Name = "RadioButton_GioiTinh_Nu";
            this.RadioButton_GioiTinh_Nu.Size = new System.Drawing.Size(39, 17);
            this.RadioButton_GioiTinh_Nu.TabIndex = 13;
            this.RadioButton_GioiTinh_Nu.TabStop = true;
            this.RadioButton_GioiTinh_Nu.Tag = "2";
            this.RadioButton_GioiTinh_Nu.Text = "Nữ";
            this.RadioButton_GioiTinh_Nu.UseVisualStyleBackColor = true;
            // 
            // Panel_GioiTinh
            // 
            this.Panel_GioiTinh.Controls.Add(this.RadioButton_GioiTinh_Nam);
            this.Panel_GioiTinh.Controls.Add(this.RadioButton_GioiTinh_Nu);
            this.Panel_GioiTinh.Location = new System.Drawing.Point(90, 384);
            this.Panel_GioiTinh.Margin = new System.Windows.Forms.Padding(0);
            this.Panel_GioiTinh.Name = "Panel_GioiTinh";
            this.Panel_GioiTinh.Size = new System.Drawing.Size(154, 36);
            this.Panel_GioiTinh.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(475, 276);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Địa chỉ";
            // 
            // TextBox_DiaChi
            // 
            this.TextBox_DiaChi.Location = new System.Drawing.Point(521, 273);
            this.TextBox_DiaChi.Multiline = true;
            this.TextBox_DiaChi.Name = "TextBox_DiaChi";
            this.TextBox_DiaChi.Size = new System.Drawing.Size(267, 46);
            this.TextBox_DiaChi.TabIndex = 15;
            this.TextBox_DiaChi.Text = "\r\n";
            // 
            // Button_Them
            // 
            this.Button_Them.Location = new System.Drawing.Point(617, 328);
            this.Button_Them.Name = "Button_Them";
            this.Button_Them.Size = new System.Drawing.Size(75, 23);
            this.Button_Them.TabIndex = 18;
            this.Button_Them.Text = "Thêm";
            this.Button_Them.UseVisualStyleBackColor = true;
            this.Button_Them.Click += new System.EventHandler(this.Button_Them_Click);
            // 
            // Button_Xoa
            // 
            this.Button_Xoa.BackColor = System.Drawing.Color.Crimson;
            this.Button_Xoa.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.Button_Xoa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_Xoa.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Button_Xoa.Location = new System.Drawing.Point(713, 329);
            this.Button_Xoa.Margin = new System.Windows.Forms.Padding(0);
            this.Button_Xoa.Name = "Button_Xoa";
            this.Button_Xoa.Size = new System.Drawing.Size(75, 21);
            this.Button_Xoa.TabIndex = 19;
            this.Button_Xoa.Text = "Xóa";
            this.Button_Xoa.UseVisualStyleBackColor = false;
            this.Button_Xoa.Click += new System.EventHandler(this.Button_Xoa_Click);
            // 
            // Button_CapNhat
            // 
            this.Button_CapNhat.Enabled = false;
            this.Button_CapNhat.Location = new System.Drawing.Point(521, 327);
            this.Button_CapNhat.Name = "Button_CapNhat";
            this.Button_CapNhat.Size = new System.Drawing.Size(75, 23);
            this.Button_CapNhat.TabIndex = 20;
            this.Button_CapNhat.Text = "Cập nhật";
            this.Button_CapNhat.UseVisualStyleBackColor = true;
            this.Button_CapNhat.Click += new System.EventHandler(this.Button_CapNhat_Click);
            // 
            // TextBox_TimKiem
            // 
            this.TextBox_TimKiem.Location = new System.Drawing.Point(286, 240);
            this.TextBox_TimKiem.Name = "TextBox_TimKiem";
            this.TextBox_TimKiem.Size = new System.Drawing.Size(192, 20);
            this.TextBox_TimKiem.TabIndex = 21;
            // 
            // Button_TimKiem
            // 
            this.Button_TimKiem.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Button_TimKiem.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.Button_TimKiem.FlatAppearance.MouseDownBackColor = System.Drawing.Color.RoyalBlue;
            this.Button_TimKiem.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.Button_TimKiem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_TimKiem.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_TimKiem.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.Button_TimKiem.Image = global::QuanLyCuaHangDienThoai.Properties.Resources.icon_search_white;
            this.Button_TimKiem.Location = new System.Drawing.Point(477, 240);
            this.Button_TimKiem.Name = "Button_TimKiem";
            this.Button_TimKiem.Size = new System.Drawing.Size(27, 20);
            this.Button_TimKiem.TabIndex = 22;
            this.Button_TimKiem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Button_TimKiem.UseVisualStyleBackColor = false;
            this.Button_TimKiem.Click += new System.EventHandler(this.Button_TimKiem_Click);
            // 
            // Button_LapDon
            // 
            this.Button_LapDon.BackColor = System.Drawing.Color.CadetBlue;
            this.Button_LapDon.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.Button_LapDon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_LapDon.ForeColor = System.Drawing.SystemColors.Menu;
            this.Button_LapDon.Location = new System.Drawing.Point(521, 358);
            this.Button_LapDon.Name = "Button_LapDon";
            this.Button_LapDon.Size = new System.Drawing.Size(75, 23);
            this.Button_LapDon.TabIndex = 23;
            this.Button_LapDon.Text = "Lập đơn hàng";
            this.Button_LapDon.UseVisualStyleBackColor = false;
            this.Button_LapDon.Click += new System.EventHandler(this.Button_LapDon_Click);
            // 
            // QuanlyKH
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Button_LapDon);
            this.Controls.Add(this.Button_TimKiem);
            this.Controls.Add(this.TextBox_TimKiem);
            this.Controls.Add(this.Button_CapNhat);
            this.Controls.Add(this.Button_Xoa);
            this.Controls.Add(this.Button_Them);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TextBox_DiaChi);
            this.Controls.Add(this.Panel_GioiTinh);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TextBox_Email);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TextBox_ID);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TextBox_DienThoai);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TextBox_HoTen);
            this.Controls.Add(this.DataGridView_KhachHang);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "QuanlyKH";
            this.Text = "Quản lý khách hàng";
            this.Load += new System.EventHandler(this.QuanLy_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_KhachHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.khachHangBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.quanLyCuaHangBanDienThoaiDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.quanLyCuaHangBanDienThoaiDataSet)).EndInit();
            this.Panel_GioiTinh.ResumeLayout(false);
            this.Panel_GioiTinh.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DataGridView_KhachHang;
        private System.Windows.Forms.TextBox TextBox_HoTen;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.BindingSource quanLyCuaHangBanDienThoaiDataSetBindingSource;
        private QuanLyCuaHangBanDienThoaiDataSet quanLyCuaHangBanDienThoaiDataSet;
        private System.Windows.Forms.BindingSource khachHangBindingSource;
        private QuanLyCuaHangBanDienThoaiDataSetTableAdapters.KhachHangTableAdapter khachHangTableAdapter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TextBox_DienThoai;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TextBox_ID;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TextBox_Email;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton RadioButton_GioiTinh_Nam;
        private System.Windows.Forms.RadioButton RadioButton_GioiTinh_Nu;
        private System.Windows.Forms.Panel Panel_GioiTinh;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TextBox_DiaChi;
        private System.Windows.Forms.Button Button_Them;
        private System.Windows.Forms.Button Button_Xoa;
        private System.Windows.Forms.Button Button_CapNhat;
        private System.Windows.Forms.TextBox TextBox_TimKiem;
        private System.Windows.Forms.Button Button_TimKiem;
        private System.Windows.Forms.Button Button_LapDon;
    }
}