﻿using System;
using System.Data;
using System.Windows.Forms;

namespace QuanLyCuaHangDienThoai.Pages.KhachHang
{
    public partial class QuanlyKH : Form
    {
        public QuanlyKH()
        {
            InitializeComponent();
        }

        private string[] GetAllFormData()
        {
            string[] args = new string[6];

            string gioiTinh;
            if (RadioButton_GioiTinh_Nam.Checked)
            {
                gioiTinh = RadioButton_GioiTinh_Nam.Tag.ToString();
            }
            else
            {
                gioiTinh = RadioButton_GioiTinh_Nu.Tag.ToString();
            }
            args[0] = this.TextBox_ID.Text;
            args[1] = this.TextBox_HoTen.Text;
            args[2] = this.TextBox_DienThoai.Text;
            args[3] = this.TextBox_Email.Text;
            args[4] = this.TextBox_DiaChi.Text;
            args[5] = gioiTinh;

            return args;
        }

        private void DataGridView_KhachHang_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1) return; // Tranh nguoi dung an vao tieu de bang

            DataGridViewRow row = DataGridView_KhachHang.Rows[e.RowIndex];

            string id               = row.Cells[0].Value.ToString();
            string hoTen            = row.Cells[1].Value.ToString();
            string dienThoai        = row.Cells[2].Value.ToString();
            string email            = row.Cells[3].Value.ToString();
            string diaChi           = row.Cells[4].Value.ToString();
            string gioiTinh         = row.Cells[5].Value.ToString();

            this.TextBox_ID.Text = id;
            this.TextBox_HoTen.Text = hoTen;
            this.TextBox_DienThoai.Text = dienThoai;
            this.TextBox_Email.Text = email;
            this.TextBox_DiaChi.Text = diaChi;
            if (this.RadioButton_GioiTinh_Nam.Text.Equals(gioiTinh))
            {
                this.RadioButton_GioiTinh_Nam.Checked = true;
                this.RadioButton_GioiTinh_Nu.Checked = false;
            }
            else
            {
                this.RadioButton_GioiTinh_Nam.Checked = false;
                this.RadioButton_GioiTinh_Nu.Checked = true;
            }

            if (id != "")
            {
                this.Button_LapDon.Enabled = true;
                this.Button_CapNhat.Enabled = true;
                this.Button_Them.Enabled = false;
            }
            else
            {
                this.Button_LapDon.Enabled = false;
                this.Button_CapNhat.Enabled = false;
                this.Button_Them.Enabled = true;
            }
            
        }

        private void FetchDataKhachHang()
        {
            DataTable khachHangDT = new Model.KhachHang().GetKhachHangDataTable();
            this.DataGridView_KhachHang.DataSource = khachHangDT;
        }

        private void QuanLy_Load(object sender, EventArgs e)
        {
            this.Button_LapDon.Enabled = false;
            FetchDataKhachHang();
        }

        private void QuanLy_Shown(Object sender, EventArgs e)
        {

            MessageBox.Show("You are in the Form.Shown event.");

        }

        private void Button_CapNhat_Click(object sender, EventArgs e)
        {
            Model.KhachHang khachHang = new Model.KhachHang();
            string[] args = GetAllFormData();

            try
            {
                bool result = khachHang.Update(args);
                if (result)
                {
                    MessageBox.Show("Đã lưu.");
                    FetchDataKhachHang();
                }
            }
            catch (Exception bug)
            {
                MessageBox.Show(bug.ToString());
            }   
        }

        private void Button_Them_Click(object sender, EventArgs e)
        {
            Model.KhachHang khachHang = new Model.KhachHang();
            string[] args = GetAllFormData();

            try
            {
                bool result = khachHang.Insert(args);
                if (result)
                {
                    MessageBox.Show("Đã Thêm.");
                    FetchDataKhachHang();
                }
            }
            catch (Exception bug)
            {
                MessageBox.Show(bug.ToString());
            }
           
        }

        private void Button_Xoa_Click(object sender, EventArgs e)
        {
            Model.KhachHang khachHang = new Model.KhachHang();
            try
            {
                bool result = khachHang.Delete(this.TextBox_ID.Text);
                if (result)
                {
                    MessageBox.Show("Đã Xóa.");
                    FetchDataKhachHang();
                }
            }
            catch (Exception bug)
            {
                MessageBox.Show(bug.ToString());
            }
        }

        private void Button_TimKiem_Click(object sender, EventArgs e)
        {
            Model.KhachHang khachHang = new Model.KhachHang();
            DataTable result = khachHang.GetKetQuaTimKiemDataTable( this.TextBox_TimKiem.Text );
            this.DataGridView_KhachHang.DataSource = result;
        }

        private void Button_LapDon_Click(object sender, EventArgs e)
        {
            MasterLayout.GetInstance().Navigate( new Pages.KhachHang.LapDon(this.TextBox_ID.Text) );
        }
    }
}
