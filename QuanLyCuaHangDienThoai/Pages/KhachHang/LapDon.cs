﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyCuaHangDienThoai.Pages.KhachHang
{
    public partial class LapDon : Form
    {
        private Model.KhachHang KhachHang;
        private Model.DonHang DonHang;
        private string IdDonHang = "";
        private DataTable _OriginalDataTable;
        private DataTable _DataTable;
        private int TongGiaTriDonHang;

        private bool IsEditing()
        {
            return DonHang != null;
        }

        public LapDon()
        {
            MessageBox.Show("Bạn chưa chọn khách hàng để lập đơn!");
            MasterLayout.GetInstance().Navigate(new Pages.KhachHang.LapDon());
            return;
        }

        public LapDon(string idKhach, string idDonHang = "")
        {
            InitializeComponent();

            if (idKhach.Equals(""))
            {
                return;
            }

            KhachHang = new Model.KhachHang().Forge(idKhach);

            if (idDonHang == "")
            {
                this.Text = "Đơn hàng mới";
            }
            else
            {
                this.DonHang = new Model.DonHang().Forge(idDonHang);
                this.IdDonHang = this.DonHang.Id;
                this.Text = "Sửa đơn hàng #" + idDonHang;
            }

            this.Text += " - " + KhachHang.HoTen;
        }

        private void LapDon_Load(object sender, EventArgs e)
        {
            _OriginalDataTable = new Model.ChiTietDonHang().GetChiTietDonHangDataTableById(IdDonHang);
            _DataTable = _OriginalDataTable;
            this.DataGridView_DonHang.DataSource = _DataTable;

            // Set danh sach dien thoai
            this.ComboBox_DienThoai.DisplayMember = "ten";
            this.ComboBox_DienThoai.ValueMember = "id";
            this.ComboBox_DienThoai.DataSource = new Model.DienThoai().GetDienThoaiDataTableRaw();

            // Set du lieu cho trang thai don hang
            this.ComboBox_TrangThai.ValueMember = "id";
            this.ComboBox_TrangThai.DisplayMember = "ten";
            this.ComboBox_TrangThai.DataSource = new Model.TrangThaiDonHang().GetTrangThaiDataTable();

            if (IsEditing())
            {
                Load_EditingFormData();
            }

            Refresh_ThongTinHoaDon();
        }

        private void Load_EditingFormData()
        {
            this.ComboBox_DienThoai.SelectedIndex = 0;
            this.NumericBox_SoLuong.Value = Convert.ToDecimal(this._DataTable.Rows[0][3].ToString());
            this.NumericBox_TienKhachDua.Value = Convert.ToDecimal(DonHang.TienKhachDua);
            this.ComboBox_TrangThai.SelectedValue = DonHang.TrangThaiId;
            this.RichTextBox_GhiChu.Text = DonHang.GhiChu;
        }

        private void Refresh_ThongTinHoaDon()
        {
            Model.NhanVien loggedInNhanVien = MasterLayout.GetInstance().LoggedInNhanVien;
            this.Label_NguoiLap.Text = loggedInNhanVien.HoTen;
            this.Label_KhachHang.Text = this.KhachHang.HoTen;

            int tongGia = 0;

            for (int i = 0; i < this._DataTable.Rows.Count; i++)
            {
                int gia = int.Parse(this._DataTable.Rows[i][2].ToString());
                int soLuong = int.Parse(this._DataTable.Rows[i][3].ToString());
                tongGia += gia * soLuong;
            }

            TongGiaTriDonHang = tongGia;
            this.Label_TongGiaTri.Text = Model.DienThoai.GetGiaText(tongGia);
        }

        private void Button_ThemVaoDon_Click(object sender, EventArgs e)
        {
            string idDienThoai = this.ComboBox_DienThoai.SelectedValue.ToString();
            Model.DienThoai dienThoai = new Model.DienThoai().Forge(idDienThoai);

            string soLuong = this.NumericBox_SoLuong.Value.ToString();
            string tenDienThoai = dienThoai.Ten;
            string giaDienThoai = dienThoai.Gia;

            if (idDienThoai.Equals(""))
            {
                MessageBox.Show("Bạn chưa chọn điện thoại");
                return;
            }

            if (int.Parse(soLuong) < 1)
            {
                MessageBox.Show("Bạn chưa nhập số lượng");
                return;
            }

            bool isDienThoaiExist = false;
            int existRowIndex = -1;

            for (int i = 0; i < this._DataTable.Rows.Count; i++)
            {
                string checkId = this._DataTable.Rows[i][0].ToString();
                if (checkId.Equals(idDienThoai))
                {
                    isDienThoaiExist = true;
                    existRowIndex = i;
                    break;
                }
            }

            if (!isDienThoaiExist && existRowIndex == -1)
            {
                DataRow newRow = this._DataTable.NewRow();

                newRow[0] = idDienThoai;
                newRow[1] = tenDienThoai;
                newRow[2] = giaDienThoai;
                newRow[3] = soLuong;
                this._DataTable.Rows.Add(newRow);
            }
            else
            {
                this._DataTable.Rows[existRowIndex][3] = soLuong;
            }
            this._DataTable.AcceptChanges();

            Refresh_ThongTinHoaDon();
        }

        private void DataGridView_DonHang_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }

            DataGridViewRow row = DataGridView_DonHang.Rows[e.RowIndex];
            string idDienThoai = row.Cells[0].Value.ToString();
            string tenDienThoai = row.Cells[1].Value.ToString();
            string soLuong = row.Cells[3].Value.ToString();
            soLuong = (soLuong == "") ? "0" : soLuong;

            if (idDienThoai == "")
            {
                this.PictureBox_DienThoai.Image = this.PictureBox_DienThoai.InitialImage;
            }
            else
            {
                Model.DienThoai dt = new Model.DienThoai();
                this.PictureBox_DienThoai.Image = dt.GetImageById(idDienThoai);
            }
            this.ComboBox_DienThoai.SelectedIndex = this.ComboBox_DienThoai.FindStringExact(tenDienThoai);
            this.NumericBox_SoLuong.Value = int.Parse(soLuong);
        }

        private object[] FetchAllDonHangData()
        {
            object[] args = new object[9];
            Model.NhanVien nv = MasterLayout.GetInstance().LoggedInNhanVien;

            args[0] = this.IdDonHang;
            args[1] = DateTime.Now;
            args[2] = this.KhachHang.Id;
            args[3] = nv.Id.ToString();
            args[4] = this.TongGiaTriDonHang.ToString();
            args[5] = this.ComboBox_TrangThai.SelectedValue.ToString();
            args[6] = this.NumericBox_TienKhachDua.Value.ToString();
            args[7] = this.RichTextBox_GhiChu.Text != "" ? this.RichTextBox_GhiChu.Text : "";
            return args;
        }

        private object[] FetchAllChiTietDonHangData(int rowIndex, int idDon = 0)
        {
            object[] args = new object[3];
            if (IsEditing())
            {
                args[0] = DonHang.Id;
            }
            else
            {
                args[0] = idDon;
            }
            
            args[1] = int.Parse(this._DataTable.Rows[rowIndex][0].ToString());
            args[2] = int.Parse(this._DataTable.Rows[rowIndex][3].ToString());
            return args;
        }
        
        private void Button_LuuDonHang_Click(object sender, EventArgs e)
        {
            // Them don hang
            if (this.IdDonHang == "")
            {
                DoInsert();
            }
            else
            {
                DoUpdate();
            }
        }

        private void DoInsert()
        {
            Model.DonHang dh = new Model.DonHang();
            try
            {
                int inserted_id = dh.Insert(FetchAllDonHangData());

                if (!(inserted_id > 0))
                {
                    MessageBox.Show("Có lỗi xảy ra");
                    return;
                }

                Model.ChiTietDonHang ctdh = new Model.ChiTietDonHang();

                for (int i = 0; i < this._DataTable.Rows.Count; i++)
                {
                    object[] args = FetchAllChiTietDonHangData(i, inserted_id);
                    ctdh.Insert(args);
                }

                MessageBox.Show("Đã lưu!");
                MasterLayout.GetInstance().Navigate(new KhachHang.DonHang());
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
            
        }

        private void DoUpdate()
        { 
            try
            {
                bool result = DonHang.Update(FetchAllDonHangData());

                if (!result)
                {
                    MessageBox.Show("Có lỗi xảy ra");
                    return;
                }

                Model.ChiTietDonHang ctdh = new Model.ChiTietDonHang();
                ctdh.Delete(DonHang.Id);

                for (int i = 0; i < this._DataTable.Rows.Count; i++)
                {
                    object[] args = FetchAllChiTietDonHangData(i);
                    ctdh.Insert(args);
                }

                MessageBox.Show("Đã lưu!");
                MasterLayout.GetInstance().Navigate(new KhachHang.DonHang());
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }

        }
    }
}