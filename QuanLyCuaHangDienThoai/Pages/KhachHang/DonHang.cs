﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyCuaHangDienThoai.Pages.KhachHang
{
    public partial class DonHang : Form
    {
        private string SelectingDonHangId;
        private string SelectingKhachHangId;
        public DonHang()
        {
            InitializeComponent();
        }

        private void EnableButtons()
        {
            Button_XemDonHang.Enabled = true;
            Button_Xoa.Enabled = true;
        }

        private void DisableButtons()
        {
            Button_XemDonHang.Enabled = false;
            Button_Xoa.Enabled = false;
        }

        private void DonHang_Load(object sender, EventArgs e)
        {
            DisableButtons();
            FetchDonHangDataTable();
        }

        private void FetchDonHangDataTable()
        {
            this.DataGridView_DonHang.DataSource = new Model.DonHang().GetDonHangDataTable();
        }

        private void DataGridView_DonHang_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1) return; // Tranh nguoi dung an vao tieu de bang

            DataGridViewRow row = DataGridView_DonHang.Rows[e.RowIndex];
            SelectingDonHangId = row.Cells[0].Value.ToString();
            SelectingKhachHangId = row.Cells[1].Value.ToString();

            if (SelectingDonHangId != "")
            {
                EnableButtons();
            }
            else
            {
                DisableButtons();
            }
        }

        private void Button_XemDonHang_Click(object sender, EventArgs e)
        {
            MasterLayout.GetInstance().Navigate(new Pages.KhachHang.LapDon(SelectingKhachHangId, SelectingDonHangId));
        }

        private void Button_Xoa_Click(object sender, EventArgs e)
        {
            Model.DonHang dh = new Model.DonHang();
            try
            {
                dh.Delete(SelectingDonHangId);
                MessageBox.Show("Đã xóa!");
                FetchDonHangDataTable();

                DisableButtons();
            }
            catch (Exception bug)
            {
                MessageBox.Show(bug.ToString());
            }
            
        }

        private void Button_TimKiem_Click(object sender, EventArgs e)
        {
            Model.DonHang dh = new Model.DonHang();
            this.DataGridView_DonHang.DataSource = dh.GetKetQuaTimKiemDataTable(this.TextBox_TimKiem.Text);
            DisableButtons();
        }
    }
}