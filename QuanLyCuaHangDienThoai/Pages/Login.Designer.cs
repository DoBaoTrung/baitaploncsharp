﻿namespace QuanLyCuaHangDienThoai.Pages
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.TextBox_TenDangNhap = new System.Windows.Forms.TextBox();
            this.TextBox_MatKhau = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Button_DangNhap = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(79, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tên đăng nhập";
            // 
            // TextBox_TenDangNhap
            // 
            this.TextBox_TenDangNhap.Location = new System.Drawing.Point(165, 67);
            this.TextBox_TenDangNhap.Name = "TextBox_TenDangNhap";
            this.TextBox_TenDangNhap.Size = new System.Drawing.Size(206, 20);
            this.TextBox_TenDangNhap.TabIndex = 1;
            this.TextBox_TenDangNhap.TextChanged += new System.EventHandler(this.TextBox_TenDangNhap_Changed);
            // 
            // TextBox_MatKhau
            // 
            this.TextBox_MatKhau.Location = new System.Drawing.Point(165, 99);
            this.TextBox_MatKhau.Name = "TextBox_MatKhau";
            this.TextBox_MatKhau.PasswordChar = '*';
            this.TextBox_MatKhau.Size = new System.Drawing.Size(206, 20);
            this.TextBox_MatKhau.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(107, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Mật khẩu";
            // 
            // Button_DangNhap
            // 
            this.Button_DangNhap.Location = new System.Drawing.Point(164, 130);
            this.Button_DangNhap.Name = "Button_DangNhap";
            this.Button_DangNhap.Size = new System.Drawing.Size(75, 23);
            this.Button_DangNhap.TabIndex = 4;
            this.Button_DangNhap.Text = "Đăng nhập";
            this.Button_DangNhap.UseVisualStyleBackColor = true;
            this.Button_DangNhap.Click += new System.EventHandler(this.Button_DangNhap_Click);
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(446, 223);
            this.Controls.Add(this.Button_DangNhap);
            this.Controls.Add(this.TextBox_MatKhau);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TextBox_TenDangNhap);
            this.Controls.Add(this.label1);
            this.Name = "Login";
            this.Text = "Đăng nhập";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TextBox_TenDangNhap;
        private System.Windows.Forms.TextBox TextBox_MatKhau;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Button_DangNhap;
    }
}