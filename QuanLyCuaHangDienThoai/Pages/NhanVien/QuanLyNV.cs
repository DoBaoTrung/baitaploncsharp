﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyCuaHangDienThoai.Pages.NhanVien
{
    public partial class QuanlyNV : Form
    {
        public QuanlyNV()
        {
            InitializeComponent();
        }

        private void DataGridView_NhanVien_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1) return; // Tranh nguoi dung an vao tieu de bang

            DataGridViewRow row = DataGridView_NhanVien.Rows[e.RowIndex];
            string id = row.Cells[0].Value.ToString();
            string hoTen = row.Cells[1].Value.ToString();
            string gioiTinh = row.Cells[2].Value.ToString();
            string dienThoai = row.Cells[3].Value.ToString();
            string dateTimeStr = row.Cells[4].Value.ToString().Equals("") ? "01/01/1970" : row.Cells[4].Value.ToString();
            string diaChi = row.Cells[5].Value.ToString();
            decimal LuongCoBan;
            decimal.TryParse(row.Cells[6].Value.ToString(), out LuongCoBan);
            string chucVu = row.Cells[7].Value.ToString();

            this.TextBox_Id.Text = id;
            this.TextBox_HoTen.Text = hoTen;
            if (RadioButton_Nam.Text.Equals(gioiTinh))
            {
                this.RadioButton_Nam.Checked = true;
                this.RadioButton_Nu.Checked = false;
            }
            else
            {
                this.RadioButton_Nam.Checked = false;
                this.RadioButton_Nu.Checked = true;
            }
            this.TextBox_DienThoai.Text = dienThoai;
            this.DateTimePicker.Value = Convert.ToDateTime(dateTimeStr);
            this.TextBox_DiaChi.Text = diaChi;
            this.ComboBox_ChucVu.SelectedIndex = this.ComboBox_ChucVu.FindStringExact(chucVu);
            this.NumericBox_LuongCoBan.Value = LuongCoBan;

            if (id != "")
            {
                this.Button_CapNhat.Enabled = true;
                this.Button_Them.Enabled = false;
            }
            else
            {
                this.Button_CapNhat.Enabled = false;
                this.Button_Them.Enabled = true;
            }
        }

        private object[] GetAllFormData()
        {
            object[] args = new object[8];

            string gioiTinh;
            if (RadioButton_Nam.Checked)
            {
                gioiTinh = RadioButton_Nam.Tag.ToString();
            }
            else
            {
                gioiTinh = RadioButton_Nu.Tag.ToString();
            }
            args[0] = this.TextBox_Id.Text;
            args[1] = this.TextBox_HoTen.Text;
            args[2] = gioiTinh;
            args[3] = this.TextBox_DienThoai.Text;
            args[4] = this.DateTimePicker.Value.ToString("yyyy-MM-dd");
            args[5] = this.TextBox_DiaChi.Text;
            args[6] = this.ComboBox_ChucVu.SelectedValue.ToString();
            args[7] = this.NumericBox_LuongCoBan.Value.ToString();

            return args;
        }

        private void FetchDataGridViewNhanVien()
        {
            Model.NhanVien nv = new Model.NhanVien();
            this.DataGridView_NhanVien.DataSource = nv.GetNhanVienDataTable();
        }

        private void FetchDataComboBoxChucVu()
        {
            Model.ChucVu cv = new Model.ChucVu();
            this.ComboBox_ChucVu.DataSource = cv.GetChucVuDataTable();
            this.ComboBox_ChucVu.DisplayMember = "ten";
            this.ComboBox_ChucVu.ValueMember = "id";
        }

        private void QuanLy_Load(object sender, EventArgs e)
        {
            FetchDataGridViewNhanVien();
            FetchDataComboBoxChucVu();
        }

        private void Button_Xoa_Click(object sender, EventArgs e)
        {
            Model.NhanVien nv = new Model.NhanVien();
            try
            {
                if (nv.Delete(this.TextBox_Id.Text))
                {
                    MessageBox.Show("Đã xóa");
                    FetchDataGridViewNhanVien();
                }
            }
            catch (Exception bug)
            {
                MessageBox.Show(bug.ToString());
            }
            
        }

        private bool ValidateFormData()
        {
            return true;
        }

        private void Button_CapNhat_Click(object sender, EventArgs e)
        {
            if (ValidateFormData() == false)
            {
                return;
            }

            Model.NhanVien nv = new Model.NhanVien();

            try
            {
                bool result = nv.Update(GetAllFormData());
                if (result)
                {
                    MessageBox.Show("Đã lưu.");
                    FetchDataGridViewNhanVien();
                }
            }
            catch (Exception bug)
            {
                MessageBox.Show(bug.ToString());
            }
        }

        private void Button_Them_Click(object sender, EventArgs e)
        {
            if (ValidateFormData() == false)
            {
                return;
            }

            Model.NhanVien nv = new Model.NhanVien();
            try
            {
                bool result = nv.Insert(GetAllFormData());
                if (result)
                {
                    MessageBox.Show("Đã Thêm.");
                    FetchDataGridViewNhanVien();
                }
            }
            catch (Exception bug)
            {
                MessageBox.Show(bug.ToString());
            }
        }

        private void Button_TimKiem_Click(object sender, EventArgs e)
        {
            Model.NhanVien nv = new Model.NhanVien();
            this.DataGridView_NhanVien.DataSource = nv.GetNhanVienSearchDataTable(this.TextBox_TimKiem.Text);
        }
    }
}