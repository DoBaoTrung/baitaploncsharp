﻿namespace QuanLyCuaHangDienThoai.Pages.NhanVien
{
    partial class QuanlyNV
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DataGridView_NhanVien = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TextBox_Id = new System.Windows.Forms.TextBox();
            this.TextBox_HoTen = new System.Windows.Forms.TextBox();
            this.TextBox_DienThoai = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.DateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.RadioButton_Nu = new System.Windows.Forms.RadioButton();
            this.RadioButton_Nam = new System.Windows.Forms.RadioButton();
            this.TextBox_DiaChi = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.ComboBox_ChucVu = new System.Windows.Forms.ComboBox();
            this.Button_TimKiem = new System.Windows.Forms.Button();
            this.TextBox_TimKiem = new System.Windows.Forms.TextBox();
            this.Button_CapNhat = new System.Windows.Forms.Button();
            this.Button_Xoa = new System.Windows.Forms.Button();
            this.Button_Them = new System.Windows.Forms.Button();
            this.NumericBox_LuongCoBan = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_NhanVien)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericBox_LuongCoBan)).BeginInit();
            this.SuspendLayout();
            // 
            // DataGridView_NhanVien
            // 
            this.DataGridView_NhanVien.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView_NhanVien.Location = new System.Drawing.Point(22, 13);
            this.DataGridView_NhanVien.Name = "DataGridView_NhanVien";
            this.DataGridView_NhanVien.ReadOnly = true;
            this.DataGridView_NhanVien.Size = new System.Drawing.Size(755, 199);
            this.DataGridView_NhanVien.TabIndex = 0;
            this.DataGridView_NhanVien.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView_NhanVien_CellClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 310);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Họ tên";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(54, 281);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(18, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "ID";
            // 
            // TextBox_Id
            // 
            this.TextBox_Id.Location = new System.Drawing.Point(80, 278);
            this.TextBox_Id.Name = "TextBox_Id";
            this.TextBox_Id.ReadOnly = true;
            this.TextBox_Id.Size = new System.Drawing.Size(80, 20);
            this.TextBox_Id.TabIndex = 3;
            // 
            // TextBox_HoTen
            // 
            this.TextBox_HoTen.Location = new System.Drawing.Point(80, 307);
            this.TextBox_HoTen.Name = "TextBox_HoTen";
            this.TextBox_HoTen.Size = new System.Drawing.Size(181, 20);
            this.TextBox_HoTen.TabIndex = 4;
            // 
            // TextBox_DienThoai
            // 
            this.TextBox_DienThoai.Location = new System.Drawing.Point(80, 337);
            this.TextBox_DienThoai.Name = "TextBox_DienThoai";
            this.TextBox_DienThoai.Size = new System.Drawing.Size(181, 20);
            this.TextBox_DienThoai.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 340);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Điện thoại";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(513, 303);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Ngày sinh";
            // 
            // DateTimePicker
            // 
            this.DateTimePicker.CustomFormat = "dd/MM/yyyy";
            this.DateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DateTimePicker.Location = new System.Drawing.Point(573, 300);
            this.DateTimePicker.Name = "DateTimePicker";
            this.DateTimePicker.Size = new System.Drawing.Size(204, 20);
            this.DateTimePicker.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(521, 276);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Giới tính";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.RadioButton_Nu);
            this.panel1.Controls.Add(this.RadioButton_Nam);
            this.panel1.Location = new System.Drawing.Point(572, 273);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(171, 25);
            this.panel1.TabIndex = 10;
            // 
            // RadioButton_Nu
            // 
            this.RadioButton_Nu.AutoSize = true;
            this.RadioButton_Nu.Location = new System.Drawing.Point(56, 2);
            this.RadioButton_Nu.Name = "RadioButton_Nu";
            this.RadioButton_Nu.Size = new System.Drawing.Size(39, 17);
            this.RadioButton_Nu.TabIndex = 1;
            this.RadioButton_Nu.TabStop = true;
            this.RadioButton_Nu.Tag = "2";
            this.RadioButton_Nu.Text = "Nữ";
            this.RadioButton_Nu.UseVisualStyleBackColor = true;
            // 
            // RadioButton_Nam
            // 
            this.RadioButton_Nam.AutoSize = true;
            this.RadioButton_Nam.Location = new System.Drawing.Point(3, 2);
            this.RadioButton_Nam.Name = "RadioButton_Nam";
            this.RadioButton_Nam.Size = new System.Drawing.Size(47, 17);
            this.RadioButton_Nam.TabIndex = 0;
            this.RadioButton_Nam.TabStop = true;
            this.RadioButton_Nam.Tag = "1";
            this.RadioButton_Nam.Text = "Nam";
            this.RadioButton_Nam.UseVisualStyleBackColor = true;
            // 
            // TextBox_DiaChi
            // 
            this.TextBox_DiaChi.Location = new System.Drawing.Point(80, 367);
            this.TextBox_DiaChi.Multiline = true;
            this.TextBox_DiaChi.Name = "TextBox_DiaChi";
            this.TextBox_DiaChi.Size = new System.Drawing.Size(181, 38);
            this.TextBox_DiaChi.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(32, 378);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Địa chỉ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(494, 362);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Lương cơ bản";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(520, 333);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Chức vụ";
            // 
            // ComboBox_ChucVu
            // 
            this.ComboBox_ChucVu.FormattingEnabled = true;
            this.ComboBox_ChucVu.Location = new System.Drawing.Point(573, 329);
            this.ComboBox_ChucVu.Name = "ComboBox_ChucVu";
            this.ComboBox_ChucVu.Size = new System.Drawing.Size(204, 21);
            this.ComboBox_ChucVu.TabIndex = 16;
            // 
            // Button_TimKiem
            // 
            this.Button_TimKiem.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Button_TimKiem.FlatAppearance.BorderColor = System.Drawing.Color.RoyalBlue;
            this.Button_TimKiem.FlatAppearance.MouseDownBackColor = System.Drawing.Color.RoyalBlue;
            this.Button_TimKiem.FlatAppearance.MouseOverBackColor = System.Drawing.Color.RoyalBlue;
            this.Button_TimKiem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_TimKiem.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_TimKiem.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.Button_TimKiem.Image = global::QuanLyCuaHangDienThoai.Properties.Resources.icon_search_white;
            this.Button_TimKiem.Location = new System.Drawing.Point(483, 238);
            this.Button_TimKiem.Name = "Button_TimKiem";
            this.Button_TimKiem.Size = new System.Drawing.Size(24, 20);
            this.Button_TimKiem.TabIndex = 27;
            this.Button_TimKiem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Button_TimKiem.UseVisualStyleBackColor = false;
            this.Button_TimKiem.Click += new System.EventHandler(this.Button_TimKiem_Click);
            // 
            // TextBox_TimKiem
            // 
            this.TextBox_TimKiem.Location = new System.Drawing.Point(267, 238);
            this.TextBox_TimKiem.Name = "TextBox_TimKiem";
            this.TextBox_TimKiem.Size = new System.Drawing.Size(220, 20);
            this.TextBox_TimKiem.TabIndex = 26;
            // 
            // Button_CapNhat
            // 
            this.Button_CapNhat.Enabled = false;
            this.Button_CapNhat.Location = new System.Drawing.Point(572, 388);
            this.Button_CapNhat.Name = "Button_CapNhat";
            this.Button_CapNhat.Size = new System.Drawing.Size(64, 23);
            this.Button_CapNhat.TabIndex = 25;
            this.Button_CapNhat.Text = "Cập nhật";
            this.Button_CapNhat.UseVisualStyleBackColor = true;
            this.Button_CapNhat.Click += new System.EventHandler(this.Button_CapNhat_Click);
            // 
            // Button_Xoa
            // 
            this.Button_Xoa.BackColor = System.Drawing.Color.Crimson;
            this.Button_Xoa.FlatAppearance.BorderColor = System.Drawing.Color.Maroon;
            this.Button_Xoa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_Xoa.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Button_Xoa.Location = new System.Drawing.Point(720, 389);
            this.Button_Xoa.Margin = new System.Windows.Forms.Padding(0);
            this.Button_Xoa.Name = "Button_Xoa";
            this.Button_Xoa.Size = new System.Drawing.Size(57, 21);
            this.Button_Xoa.TabIndex = 24;
            this.Button_Xoa.Text = "Xóa";
            this.Button_Xoa.UseVisualStyleBackColor = false;
            this.Button_Xoa.Click += new System.EventHandler(this.Button_Xoa_Click);
            // 
            // Button_Them
            // 
            this.Button_Them.Location = new System.Drawing.Point(650, 388);
            this.Button_Them.Name = "Button_Them";
            this.Button_Them.Size = new System.Drawing.Size(55, 23);
            this.Button_Them.TabIndex = 23;
            this.Button_Them.Text = "Thêm";
            this.Button_Them.UseVisualStyleBackColor = true;
            this.Button_Them.Click += new System.EventHandler(this.Button_Them_Click);
            // 
            // NumericBox_LuongCoBan
            // 
            this.NumericBox_LuongCoBan.Increment = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.NumericBox_LuongCoBan.Location = new System.Drawing.Point(573, 358);
            this.NumericBox_LuongCoBan.Maximum = new decimal(new int[] {
            1410065408,
            2,
            0,
            0});
            this.NumericBox_LuongCoBan.Name = "NumericBox_LuongCoBan";
            this.NumericBox_LuongCoBan.Size = new System.Drawing.Size(204, 20);
            this.NumericBox_LuongCoBan.TabIndex = 28;
            this.NumericBox_LuongCoBan.ThousandsSeparator = true;
            // 
            // QuanlyNV
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.NumericBox_LuongCoBan);
            this.Controls.Add(this.Button_TimKiem);
            this.Controls.Add(this.TextBox_TimKiem);
            this.Controls.Add(this.Button_CapNhat);
            this.Controls.Add(this.Button_Xoa);
            this.Controls.Add(this.Button_Them);
            this.Controls.Add(this.ComboBox_ChucVu);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.TextBox_DiaChi);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.DateTimePicker);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TextBox_DienThoai);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TextBox_HoTen);
            this.Controls.Add(this.TextBox_Id);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DataGridView_NhanVien);
            this.Name = "QuanlyNV";
            this.Text = "Quản lý Nhân Viên";
            this.Load += new System.EventHandler(this.QuanLy_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_NhanVien)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumericBox_LuongCoBan)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DataGridView_NhanVien;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TextBox_Id;
        private System.Windows.Forms.TextBox TextBox_HoTen;
        private System.Windows.Forms.TextBox TextBox_DienThoai;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker DateTimePicker;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton RadioButton_Nu;
        private System.Windows.Forms.RadioButton RadioButton_Nam;
        private System.Windows.Forms.TextBox TextBox_DiaChi;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox ComboBox_ChucVu;
        private System.Windows.Forms.Button Button_TimKiem;
        private System.Windows.Forms.TextBox TextBox_TimKiem;
        private System.Windows.Forms.Button Button_CapNhat;
        private System.Windows.Forms.Button Button_Xoa;
        private System.Windows.Forms.Button Button_Them;
        private System.Windows.Forms.NumericUpDown NumericBox_LuongCoBan;
    }
}