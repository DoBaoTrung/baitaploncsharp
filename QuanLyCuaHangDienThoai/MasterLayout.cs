﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyCuaHangDienThoai
{
    public partial class MasterLayout : Form
    {

        public Model.NhanVien LoggedInNhanVien;

        private static MasterLayout Instance;

        public static MasterLayout GetInstance()
        {
            if (Instance == null)
            {
                Instance = new MasterLayout();
            }
            return Instance;
        }
        private MasterLayout()
        {
            InitializeComponent();
            this.Text = "Pineapple";
            Navigate(new Pages.Home());
        }

        public void AddLoggedInNhanVien(string idNhanVien)
        {
            Model.NhanVien nv = new Model.NhanVien();
            this.LoggedInNhanVien = nv.Forge(idNhanVien);
        }
       
        public void Navigate(Form targetForm)
        {

            foreach (Form childForm in this.MdiChildren)
            {
                childForm.Dispose();
            }

            targetForm.Size = Size;
            targetForm.ControlBox = false;
            targetForm.MdiParent = this;
            targetForm.Location = new Point(-10, -50);
            targetForm.WindowState = FormWindowState.Maximized;
            targetForm.Show();
        }

        /**
         * MENU FILE
         */
        private void MenuItem_File_Thoat_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(1);
        }

        private void MenuItem_File_DangXuat_Click(object sender, EventArgs e)
        {
            this.Hide();
            Pages.Login.GetInstance().Show();
        }

        private void MenuItem_KhachHang_QuanLy_Click(object sender, EventArgs e)
        {
            this.Navigate(new Pages.KhachHang.QuanlyKH());
        }

        private void MenuItem_File_TrangChu_Click(object sender, EventArgs e)
        {
            this.Navigate(new Pages.Home());
        }

        private void MenuItem_NhanVien_QuanLy_Click(object sender, EventArgs e)
        {
            this.Navigate(new Pages.NhanVien.QuanlyNV());
        }

        private void MenuItem_KhoHang_QuanLy_Click(object sender, EventArgs e)
        {
            this.Navigate(new Pages.KhoHang.QuanLyKho());
        }

        private void MenuItem_KhachHang_DonHang_Click(object sender, EventArgs e)
        {
            this.Navigate(new Pages.KhachHang.DonHang());
        }

        private void MasterLayout_Load(object sender, EventArgs e)
        {

        }

        private void MenuItem_KhoHang_BaoCao_Click(object sender, EventArgs e)
        {
            this.Navigate(new Pages.KhoHang.RpSanPham());
        }
    }
}